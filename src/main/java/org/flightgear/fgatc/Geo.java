/*
 *  Copyright (C) 2010 S.Hamilton
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc;

/**
 *
 * @author S.Hamilton
 */
public class Geo {

    /* C O N S T A N T S */
    public static final double NM2METRE = 1852;
    public static final double METRE2NM = 0.000539956803456;
    public static final double FAI_SPHERE = 6371; //km
    public static final double EARTH_RADIUS_METRE = 6378137;
    public static final double EARTH_RADIUS_KM    = 6378.137;
    public static final double EARTH_RADIUS_MILES = 3963.191;
    public static final double EARTH_RADIUS_NM    = 3441.596;



    /**
     *
     * @param nm
     * @return
     */
    public static double convertNmToRadians(double nm) {
        double radians = (Math.PI/(180*60))*nm;
        return radians;
    }

    /**
     *
     * @param rad
     * @return
     */
    public static double convertRadiansToNm(double rad) {
        double nm=((180*60)/Math.PI)*rad;
        return nm;
    }

    /**
     *
     * @param deg
     * @return
     */
    public static double convertAngleDegToRad(double deg) {
        double angleRadians=(Math.PI/180)*deg;
        return angleRadians;
    }

    /**
     *
     * @param rad
     * @return
     */
    public static double convertAngleRadiansToDeg(double rad) {
        double angleDegrees=(180/Math.PI)*rad;
        return angleDegrees;
    }

    /**
     *
     * @param rad
     * @return
     */
    public static double convertDistanceRadToKm(double rad) {
        double distanceKm = FAI_SPHERE*rad;
        return distanceKm;
    }
    
    /**
     * 
     * @param km
     * @return 
     */
    public static double convertDistanceKmToRad(double km) {
        double distanceRadians = km/FAI_SPHERE;
        return distanceRadians;
    }

    /**
     * Convert
     * @param deg
     * @param sec
     * @return
     */
    public static double convertDegSecToRad(double deg, double sec) {
        //lat1=(33+57/60)*pi/180=0.592539
        double rad = (deg+sec/60)*Math.PI/180;
        return rad;
    }


    /**
     * This is the only method that accepts decimal degress instead of radians for lat/lon
     * 
     * @param latDeg
     * @param lonDeg
     * @param radiusKm
     * @return
     */
    public static LatLon[] calculateBoundingBoxByDistanceKm(double latDeg, double lonDeg, double radiusKm) {
        // SELECT [columns] FROM [column] WHERE 1=1 AND 3963.191 * ACOS((SIN(PI() * [latitude] / 180) * SIN(PI() * [column].latitude / 180)) + (COS(PI() * [latitude] /180) * cos(PI() * [column].latitude / 180) * COS(PI() * [column].longitude / 180 - PI() * [longitude] / 180)) ) <= 10

        // $lat = $_GET['lat'];  // latitude of centre of bounding circle in degrees
        // $lon = $_GET['lon'];  // longitude of centre of bounding circle in degrees
        // $rad = $_GET['rad'];  // radius of bounding circle in kilometers
        //$R = 6371;  // earth's radius, km
        // first-cut bounding box (in degrees)
        //$maxLat = $lat + rad2deg($rad/$R);
        //$minLat = $lat - rad2deg($rad/$R);
        // compensate for degrees longitude getting smaller with increasing latitude
        //$maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
        //$minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

        // convert origin of filter circle to radians
        //$latRad = deg2rad($lat);
        //$lonRad = deg2rad($lon);

        LatLon[] retBox = new LatLon[2];
        retBox[0] = new LatLon();
        retBox[1] = new LatLon();

        double maxLat = latDeg+Geo.convertAngleRadiansToDeg(Geo.convertDistanceKmToRad(radiusKm));
        double minLat = latDeg-Geo.convertAngleRadiansToDeg(Geo.convertDistanceKmToRad(radiusKm));
        double maxLon = lonDeg+Geo.convertAngleRadiansToDeg(Geo.convertDistanceKmToRad(radiusKm)/Math.cos(Geo.convertAngleDegToRad(latDeg)));
        double minLon = lonDeg-Geo.convertAngleRadiansToDeg(Geo.convertDistanceKmToRad(radiusKm)/Math.cos(Geo.convertAngleDegToRad(latDeg)));

        retBox[0].lat = maxLat;
        retBox[0].lon = maxLon;
        retBox[1].lat = minLat;
        retBox[1].lon = minLon;

        return retBox;
    }

/**
     * This is the only method that accepts decimal degress instead of radians for lat/lon
     *
     * @param latDeg
     * @param lonDeg
     * @param radiusKm
     * @return
     */
    public static LatLon[] calculateBoundingBoxByDistanceNm(double latDeg, double lonDeg, double radiusNm) {
        // SELECT [columns] FROM [column] WHERE 1=1 AND 3963.191 * ACOS((SIN(PI() * [latitude] / 180) * SIN(PI() * [column].latitude / 180)) + (COS(PI() * [latitude] /180) * cos(PI() * [column].latitude / 180) * COS(PI() * [column].longitude / 180 - PI() * [longitude] / 180)) ) <= 10

        // $lat = $_GET['lat'];  // latitude of centre of bounding circle in degrees
        // $lon = $_GET['lon'];  // longitude of centre of bounding circle in degrees
        // $rad = $_GET['rad'];  // radius of bounding circle in kilometers
        //$R = 6371;  // earth's radius, km
        // first-cut bounding box (in degrees)
        //$maxLat = $lat + rad2deg($rad/$R);
        //$minLat = $lat - rad2deg($rad/$R);
        // compensate for degrees longitude getting smaller with increasing latitude
        //$maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
        //$minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

        // convert origin of filter circle to radians
        //$latRad = deg2rad($lat);
        //$lonRad = deg2rad($lon);

        LatLon[] retBox = new LatLon[2];
        retBox[0] = new LatLon();
        retBox[1] = new LatLon();

        double maxLat = latDeg+Geo.convertAngleRadiansToDeg(radiusNm/EARTH_RADIUS_NM);
        double minLat = latDeg-Geo.convertAngleRadiansToDeg(radiusNm/EARTH_RADIUS_NM);
        double maxLon = lonDeg+Geo.convertAngleRadiansToDeg(radiusNm/EARTH_RADIUS_NM/Math.cos(Geo.convertAngleDegToRad(latDeg)));
        double minLon = lonDeg-Geo.convertAngleRadiansToDeg(radiusNm/EARTH_RADIUS_NM/Math.cos(Geo.convertAngleDegToRad(latDeg)));

        retBox[0].lat = maxLat;
        retBox[0].lon = maxLon;
        retBox[1].lat = minLat;
        retBox[1].lon = minLon;

        return retBox;
    }




    /**
     * Convert KM distance to Radians
     *
     * @param km
     * @return
     */
    public static double convertAngularKmToRad(double km) {
        //double distanceRadians = km/FAI_SPHERE;
        double distanceRadians = (km*Math.PI/180)*EARTH_RADIUS_KM;
        return distanceRadians;
    }

    /**
     * Convert Nautical Miles to radians
     *
     * @param nm
     * @return
     */
    public static double convertDistanceNMToRad(double nm) {
        double rad = (nm*Math.PI/180)*EARTH_RADIUS_NM;
        return rad;
    }

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public static double calcGCDistanceRad(double lat1, double lon1, double lat2, double lon2) {
         //d = 2*asin(sqrt((sin((lat1-lat2)/2))^2 +cos(lat1)*cos(lat2)*(sin((lon1-lon2)/2))^2))
        
        double dRad = 2*Math.asin(Math.sqrt(Math.pow((Math.sin((lat1-lat2)/2)),2.0)+Math.cos(lat1)*Math.cos(lat2)*Math.pow(Math.sin((lon1-lon2)/2),2.0)));
        return dRad;
    }

    /**
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @return
     */
    public static double calcCourseRad(double lat1, double lon1, double lat2, double lon2) {
    //       IF sin(lon2-lon1)<0
    //tc1=acos((sin(lat2)-sin(lat1)*cos(d))/(sin(d)*cos(lat1)))
    //ELSE
    //tc1=2*pi-acos((sin(lat2)-sin(lat1)*cos(d))/(sin(d)*cos(lat1)))
    //ENDIF
        double tc1 = 0;
        double d = calcGCDistanceRad(lat1, lon1, lat2, lon2);
        if (Math.sin(lon2-lon1) < 0) {
            tc1 = Math.acos((Math.sin(lat2)-Math.sin(lat1)*Math.cos(d))/(Math.sin(d)*Math.cos(lat1)));
        } else {
            tc1 = 2*Math.PI-Math.acos((Math.sin(lat2)-Math.sin(lat1)*Math.cos(d))/(Math.sin(d)*Math.cos(lat1)));
        }
        return tc1;
    }


    /**
     * Calculate a point by a distance on a radial from point
     *
     * @param lat1
     * @param lon1
     * @param d in radians
     * @param tc in angle radians
     * @return
     */
    public static LatLon calcPointDistanceAndRadial(double lat1, double lon1, double d, double tc) {
        LatLon ret = new LatLon();
        //lat =asin(sin(lat1)*cos(d)+cos(lat1)*sin(d)*cos(tc))
        //dlon=atan2(sin(tc)*sin(d)*cos(lat1),cos(d)-sin(lat1)*sin(lat))
        //lon=mod( lon1-dlon +pi,2*pi )-pi
        ret.lat = Math.asin(Math.sin(lat1)*Math.cos(d)+Math.cos(lat1)*Math.sin(d)*Math.cos(tc));
        double dlon = Math.atan2(Math.sin(tc)*Math.sin(d)*Math.cos(lat1),Math.cos(d)-Math.sin(lat1)*Math.sin(ret.lat));
        ret.lon = (lon1-dlon+Math.PI%2*Math.PI)-Math.PI;
        return ret;
    }

    /**
     * Calculate a point from a fraction (between 0 and 1) distance between two points.
     *
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param f
     * @return
     */
    public static LatLon calcPointFractionBetween(double lat1, double lon1, double lat2, double lon2, double f) {
        LatLon ret = new LatLon();
        //A=sin((1-f)*d)/sin(d)
        //B=sin(f*d)/sin(d)
        //x = A*cos(lat1)*cos(lon1) +  B*cos(lat2)*cos(lon2)
        //y = A*cos(lat1)*sin(lon1) +  B*cos(lat2)*sin(lon2)
        //z = A*sin(lat1)           +  B*sin(lat2)
        //lat=atan2(z,sqrt(x^2+y^2))
        //lon=atan2(y,x)

        double d = calcGCDistanceRad(lat1, lon1, lat2, lon2);
        double A = Math.sin((1-f)*d)/Math.sin(d);
        double B = Math.sin(f*d)/Math.sin(d);
        double x = A*Math.cos(lat1)*Math.cos(lon1) + B*Math.cos(lat2)*Math.cos(lon2);
        double y = A*Math.cos(lat1)*Math.sin(lon1) + B*Math.cos(lat2)*Math.sin(lon2);
        double z = A*Math.sin(lat1) + B*Math.sin(lat2);
        ret.lat = Math.atan2(z, Math.sqrt(Math.pow(x, 2.0)+Math.pow(y, 2.0)));
        ret.lon = Math.atan2(y, x);

        return ret;
    }


    /**
     * 
     * @param dfDecimal
     */
    public static void Dec2DMS(double dfDecimal) {
		// define variables local to this method
                double dfDegree;
                double dfMinute;
                double dfSecond;
                //
		double dfFrac;			// fraction after decimal
                double dfSec;

		// Get degrees by chopping off at the decimal
		dfDegree = Math.floor( dfDecimal );
		// correction required since floor() is not the same as int()
		if ( dfDegree < 0 )
			dfDegree = dfDegree + 1;

		// Get fraction after the decimal
		dfFrac = Math.abs( dfDecimal - dfDegree );

		// Convert this fraction to seconds (without minutes)
		dfSec = dfFrac * 3600;

		// Determine number of whole minutes in the fraction
		dfMinute = Math.floor( dfSec / 60 );

		// Put the remainder in seconds
		dfSecond = dfSec - dfMinute * 60;

		// Fix rounoff errors
		if ( Math.rint( dfSecond ) == 60 )
		{
			dfMinute = dfMinute + 1;
			dfSecond = 0;
		}

		if ( Math.rint( dfMinute ) == 60 )
		{
			if ( dfDegree < 0 )
				dfDegree = dfDegree - 1;
			else // ( dfDegree => 0 )
				dfDegree = dfDegree + 1;

			dfMinute = 0;
		}

		return;
	}


        /**
         *
         * @param dfDegree
         * @param dfMinute
         * @param dfSecond
         * @return
         */
	public static double DMS2Dec(int dfDegree, int dfMinute, int dfSecond)  {
		// define variables local to this method
                double dfDecimal;
		double dfFrac;					// fraction after decimal

		// Determine fraction from minutes and seconds
		dfFrac = dfMinute / 60 + dfSecond / 3600;

		// Be careful to get the sign right. dfDegIn is the only signed input.
		if ( dfDegree < 0 )
			dfDecimal = dfDegree - dfFrac;
		else
			dfDecimal = dfDegree + dfFrac;

		return dfDecimal;
	}




}
