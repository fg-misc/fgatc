/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.flightgear.fgatc.ejb.dto.AirwayRecord;

/**
 *
 * @author scotth
 */
public class AwyParser {


    /* CONSTANTS */
    public static final int PARSER_VERSION = 640;
    public static final int ROW_BEGIN_INTERSECT = 0;
    public static final int ROW_BEGIN_LAT = 1;
    public static final int ROW_BEGIN_LON = 2;
    public static final int ROW_END_INTERSECT = 3;
    public static final int ROW_END_LAT = 4;
    public static final int ROW_END_LON = 5;
    public static final int ROW_LO_HI_IND = 6;
    public static final int ROW_BASE_FL = 7;
    public static final int ROW_TOP_FL = 8;
    public static final int ROW_SEG_NAME = 9;

    public HashMap<String, List<AirwayRecord>> parse(InputStream fin) {
        HashMap retList = new HashMap();
        Logger log = Logger.getLogger(this.getClass().getName() + ".parse");
        log.info("Parsing awy.dat file");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.length() > 0) {
                    log.fine("readline: " + line);
                    String[] initParts = line.split("[ \t]");
                    ArrayList tmpList = new ArrayList(Arrays.asList(initParts));
                    for(Iterator p = tmpList.iterator(); p.hasNext();){
                        String part = (String)p.next();
                        if (part.length() == 0) {
                            p.remove();
                        } else {
                            log.finer("part: >>>>" + part + "<<<<");
                        }
                    }
                    log.fine("Airway record contains "+tmpList.size()+" parts");
                    String[] parts = (String[])tmpList.toArray(new String[tmpList.size()]);
                    log.fine(("parts[0] >>>"+parts[0]+"<<<"));
                    AirwayRecord record = null;
                    
                    if (tmpList.size() == 10) {
                      record = parseAirway(parts);
                    } else {
                        log.info("Airway record contains "+tmpList.size()+" parts");
                    }

                    if (record != null) {
                        List recList = (List)retList.get(record.airwayName);
                        if (recList == null) {
                            recList = new ArrayList();
                        }
                        log.fine("Added new airway sub-record to "+record.airwayName);
                        recList.add(record);
                        retList.put(record.airwayName, recList);
                    }
                }
            }
            log.info("Completed parsing of "+retList.size()+" records");
        } catch (IOException ex) {
            Logger.getLogger(NavParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException nfe) {
            Logger.getLogger(NavParser.class.getName()).log(Level.SEVERE, null, nfe);
        }
        return retList;
    }

    private String concat(String[] parts, int start, int end) {
        StringBuffer retStr = new StringBuffer();
        for(int p = start; p != end; p++) {
            if (p > start) {
                retStr.append(" ");
            }
            retStr.append(parts[p]);
        }
        return retStr.toString();
    }

    private AirwayRecord parseAirway(String[] parts) {
        AirwayRecord record = new AirwayRecord();
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseAirway");
        record.airwayName = parts[ROW_SEG_NAME];
        log.fine("[AWY] airway name: "+record.airwayName);
        record.airwayType = Integer.valueOf(parts[ROW_LO_HI_IND]).intValue();
        record.baseFL     = Integer.valueOf(parts[ROW_BASE_FL]).intValue();
        record.beginLatDecimal = Float.valueOf(parts[ROW_BEGIN_LAT]).floatValue();
        log.fine("[AWY] begin lat: "+record.beginLatDecimal);
        record.beginLonDecimal = Float.valueOf(parts[ROW_BEGIN_LON]).floatValue();
        record.endLatDecimal   = Float.valueOf(parts[ROW_END_LAT]).floatValue();
        log.fine("[AWY] end lat: "+record.endLatDecimal);
        record.endLonDecimal   = Float.valueOf(parts[ROW_END_LON]).floatValue();
        record.intersectBegin  = parts[ROW_BEGIN_INTERSECT];
        log.fine("[AWY] intersect begin: "+record.intersectBegin);
        record.intersectEnd    = parts[ROW_END_INTERSECT];
        log.fine("[AWY] intersect end: "+record.intersectEnd);
        record.topFL           = Integer.valueOf(parts[ROW_TOP_FL]).intValue();

        return record;
    }
}
