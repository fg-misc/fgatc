/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Logger;
import org.flightgear.fgatc.ejb.dto.AptRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptCommsRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptRunwayRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptSeaRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptTowerRecord;

/**
 *
 * @author scotth
 */
public class Apt810Parser extends AptParser {

    /* Constants */
    public static final int PARSER_VERSION = 810;
    public static final int ROW_LAND_AIRPORT_HEADER = 1;
    public static final int ROW_SEAPLANE_AIRPORT_HEADER = 16;
    public static final int ROW_HELIPORT_AIRPORT_HEADER = 17;
    public static final int ROW_RUNWAY = 10;
    public static final int ROW_AIRPORT_VIEWPOINT = 14;
    public static final int ROW_AEROPLANE_STARTUP = 15;
    public static final int ROW_AIRPORT_LIGHT_BEACON = 18;
    public static final int ROW_WINDSOCK = 19;
    public static final int ROW_COMMUNICATION1 = 50;
    public static final int ROW_COMMUNICATION2 = 51;
    public static final int ROW_COMMUNICATION3 = 52;
    public static final int ROW_COMMUNICATION4 = 53;
    public static final int ROW_COMMUNICATION5 = 54;
    public static final int ROW_COMMUNICATION6 = 55;
    public static final int ROW_COMMUNICATION7 = 56;

    @Override
    public HashMap<String, AptRecord> parse(InputStream fin) {
        HashMap retList = new HashMap();
        Logger log = Logger.getLogger(this.getClass().getName() + ".parse");
        log.info("Parsing apt.dat file");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.

            AptRecord airportRecord = null;
            AptRunwayRecord runwayRecord = null;

            while (reader.ready()) {
                String line = reader.readLine();
                AptCommsRecord radio = null;
                if (line.length() > 0) {
                    log.fine("readline: " + line);
                    String[] initParts = line.split("[ \t]");
                    ArrayList tmpList = new ArrayList(Arrays.asList(initParts));
                    int pos = 0;
                    for (Iterator p = tmpList.iterator(); p.hasNext();) {
                        String part = (String) p.next();
                        if (part.length() == 0) {
                            p.remove();
                        } else {
                            log.finer("part: [" + pos + "] >>>>" + part + "<<<<");
                            pos++;
                        }
                    }
                    String[] parts = (String[]) tmpList.toArray(new String[tmpList.size()]);
                    log.fine(("parts[0] >>>" + parts[0] + "<<<"));
                    int rowType = Integer.valueOf(parts[0]).intValue();

                    log.fine("Row type: " + rowType);
                    switch (rowType) {
                        case ROW_LAND_AIRPORT_HEADER:
                            airportRecord = parseAirport(parts);
                            retList.put(((AptLandRecord) airportRecord).icaoId, airportRecord);
                            break;
                        case ROW_SEAPLANE_AIRPORT_HEADER:
                            airportRecord = parseSeaAirport(parts);
                            retList.put(((AptSeaRecord) airportRecord).icaoId, airportRecord);
                            break;
                        case ROW_RUNWAY:
                            runwayRecord = parseRunway(parts);
                            if (runwayRecord != null) {
                                if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                    ((AptLandRecord) airportRecord).runways.add(runwayRecord);
                                }
                            }
                            break;
                        case ROW_AIRPORT_VIEWPOINT:
                            AptTowerRecord tower = parseViewpoint(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).hasATC = true;
                                ((AptLandRecord)airportRecord).tower = tower;
                            }
                            break;
                        case ROW_COMMUNICATION1:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION2:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION3:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION4:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION5:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION6:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                        case ROW_COMMUNICATION7:
                            radio = parseCommunications(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).radio.add(radio);
                            }
                            break;
                    }
                }
            }
        } catch (IOException ioe) {
            log.info("I/O error in parsing apt file: " + ioe.getMessage());
        }
        log.info("Completed parsing of " + retList.size() + " records");
        return retList;
    }

    private AptRecord parseAirport(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseAirport");

        AptLandRecord record = new AptLandRecord();
        record.runways = new ArrayList();
        record.radio   = new ArrayList();
        log.fine(("parts[0] >>>" + parts[0] + "<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.elevationFt = Float.valueOf(parts[1]).floatValue();
        record.hasATC = Boolean.valueOf(parts[2]).booleanValue();
        record.icaoId = parts[4];
        record.airportName = super.concat(parts, 5, parts.length);
        log.fine("Parsed Airport record for: " + record.icaoId + " as " + record.airportName);
        return record;
    }

    private AptRecord parseSeaAirport(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseSeaAirport");
        AptSeaRecord record = new AptSeaRecord();
        log.fine(("parts[0] >>>" + parts[0] + "<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();

        return record;
    }

    private AptRunwayRecord parseRunway(String[] parts) {
        //10 -33.940645  151.176795 07x  74.12  8277 0000.0324 0000.0000   148 231221  1 2 3 0.25 1 0300.0300
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseRunway()");
        AptRunwayRecord record = null;
        if (parts[3].equals("xxx") == false) {
            record = new AptRunwayRecord();
            log.fine(("parts[0] >>>" + parts[0] + "<<<"));
            record.rowType = Integer.valueOf(parts[0]).intValue();
            record.runwayLat = Float.valueOf(parts[1]).floatValue();
            record.runwayLon = Float.valueOf(parts[2]).floatValue();
            record.runwayId = parts[3].replaceFirst("x", "");
            record.runwayHeadingDeg = Float.valueOf(parts[4]).floatValue();
            record.runwayLengthFeet = Integer.valueOf(parts[5]).intValue();
            record.runwayLengthMetres = (int)(record.runwayLengthFeet/3.2808399);
        }
        return record;
    }

    private AptRunwayRecord createOppositeRunway(AptRunwayRecord rec) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseRunway()");
        AptRunwayRecord record = new AptRunwayRecord();
        record.rowType = rec.rowType;
        float head = rec.runwayHeadingDeg + 180;
        if (head > 360) {
            head = head - 360;
        }
        record.runwayHeadingDeg = head;
        record.runwayId = String.format("%02i", record.runwayHeadingDeg / 10);
        record.runwayLengthFeet = rec.runwayLengthFeet;
        record.runwayLengthMetres = rec.runwayLengthMetres;
        return record;
    }

    private AptTowerRecord parseViewpoint(String[] parts) {
        //14  52.307490    4.762460 200 0 ATC Tower
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseViewpoint");
        AptTowerRecord record = null;
        record = new AptTowerRecord();
        for(int i = 0; i != parts.length; i++) {
            log.fine(("parts["+i+"] >>>"+parts[i]+"<<<"));
        }
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.towerLat = Float.valueOf(parts[1]).floatValue();
        record.towerLon = Float.valueOf(parts[2]).floatValue();
        record.heightFt = Double.valueOf(parts[3]).intValue();

        return record;
    }

    private AptCommsRecord parseCommunications(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseViewpoint");
        AptCommsRecord record = new AptCommsRecord();
        for(int i = 0; i != parts.length; i++) {
            log.fine(("parts["+i+"] >>>"+parts[i]+"<<<"));
        }
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.frequencyMHz = (Double.valueOf(parts[1]).doubleValue())/100;
        record.radioType = super.concat(parts, 2, parts.length);
        return record;
    }
}
