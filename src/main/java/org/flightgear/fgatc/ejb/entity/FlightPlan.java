/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.logging.Logger;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;

/**
 *
 * @author scotth
 */
@Entity
@NamedQueries({
    @NamedQuery(name="FlightPlan.findCurrentByICAO", query="select fp from FlightPlan fp where fp.departureICAO = :icao and fp.departureTime > :date order by fp.departureTime asc"),
    @NamedQuery(name="FlightPlan.findAllByUser", query="select fp from FlightPlan fp where fp.originatorUsername = :user order by fp.dateCreated desc")
})
public class FlightPlan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    private String originatorUsername;
    private String aircraftId;
    private String flightRules;
    private String flightType;  //scheduled, GA, mil, special
    private String aircraftType;
    private String departureICAO;
    private String destinationICAO;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date departureTime;
    private Double cruiseSpeed;
    private int flightLevel;  // 0 = VFR
    private String route1;
    private String route2;
    private String route3;
    private String route4;
    private String totalEET;  // in HHMM format
    private String alternateICAO;
    private String fuelOnboardTime; // in HHMM format
    private int    personsOnboard;  //crew and pax
    private String aircraftColourMarkings;
    private String pilotInCommand;
    private String planStatus;   // pending, accepted, rejected, departed, closed




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FlightPlan)) {
            return false;
        }
        FlightPlan other = (FlightPlan) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.flightgear.fgatc.ejb.entity.FlightPlan[id=" + id + "]";
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the originatorUsername
     */
    public String getOriginatorUsername() {
        return originatorUsername;
    }

    /**
     * @param originatorUsername the originatorUsername to set
     */
    public void setOriginatorUsername(String originatorUsername) {
        this.originatorUsername = originatorUsername;
    }

    /**
     * @return the aircraftId
     */
    public String getAircraftId() {
        return aircraftId;
    }

    /**
     * @param aircraftId the aircraftId to set
     */
    public void setAircraftId(String aircraftId) {
        this.aircraftId = aircraftId;
    }

    /**
     * @return the flightRules
     */
    public String getFlightRules() {
        return flightRules;
    }

    /**
     * @param flightRules the flightRules to set
     */
    public void setFlightRules(String flightRules) {
        this.flightRules = flightRules;
    }

    /**
     * @return the flightType
     */
    public String getFlightType() {
        return flightType;
    }

    /**
     * @param flightType the flightType to set
     */
    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the departureICAO
     */
    public String getDepartureICAO() {
        return departureICAO;
    }

    /**
     * @param departureICAO the departureICAO to set
     */
    public void setDepartureICAO(String departureICAO) {
        this.departureICAO = departureICAO;
    }

    /**
     * @return the destinationICAO
     */
    public String getDestinationICAO() {
        return destinationICAO;
    }

    /**
     * @param destinationICAO the destinationICAO to set
     */
    public void setDestinationICAO(String destinationICAO) {
        this.destinationICAO = destinationICAO;
    }

    /**
     * @return the departureTime
     */
    public Date getDepartureTime() {
        return departureTime;
    }

    /**
     * @param departureTime the departureTime to set
     */
    public void setDepartureTime(Date departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * @return the cruiseSpeed
     */
    public Double getCruiseSpeed() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getCruiseSpeed");
        log.info("Return cruise speed: "+this.cruiseSpeed);
        return cruiseSpeed;
    }

    /**
     * @param cruiseSpeed the cruiseSpeed to set
     */
    public void setCruiseSpeed(Double cruiseSpeed) {
        this.cruiseSpeed = cruiseSpeed;
    }

    /**
     * @return the flightLevel
     */
    public int getFlightLevel() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getFlightLevel");
        log.info("Return flightLevel: "+this.flightLevel);
        return flightLevel;
    }

    /**
     * @param flightLevel the flightLevel to set
     */
    public void setFlightLevel(int flightLevel) {
        this.flightLevel = flightLevel;
    }

    /**
     * @return the route1
     */
    public String getRoute1() {
        return route1;
    }

    /**
     * @param route1 the route1 to set
     */
    public void setRoute1(String route1) {
        this.route1 = route1;
    }

    /**
     * @return the route2
     */
    public String getRoute2() {
        return route2;
    }

    /**
     * @param route2 the route2 to set
     */
    public void setRoute2(String route2) {
        this.route2 = route2;
    }

    /**
     * @return the route3
     */
    public String getRoute3() {
        return route3;
    }

    /**
     * @param route3 the route3 to set
     */
    public void setRoute3(String route3) {
        this.route3 = route3;
    }

    /**
     * @return the route4
     */
    public String getRoute4() {
        return route4;
    }

    /**
     * @param route4 the route4 to set
     */
    public void setRoute4(String route4) {
        this.route4 = route4;
    }

    /**
     * @return the totalEET
     */
    public String getTotalEET() {
        return totalEET;
    }

    /**
     * @param totalEET the totalEET to set
     */
    public void setTotalEET(String totalEET) {
        this.totalEET = totalEET;
    }

    /**
     * @return the alternateICAO
     */
    public String getAlternateICAO() {
        return alternateICAO;
    }

    /**
     * @param alternateICAO the alternateICAO to set
     */
    public void setAlternateICAO(String alternateICAO) {
        this.alternateICAO = alternateICAO;
    }

    /**
     * @return the fuelOnboardTime
     */
    public String getFuelOnboardTime() {
        return fuelOnboardTime;
    }

    /**
     * @param fuelOnboardTime the fuelOnboardTime to set
     */
    public void setFuelOnboardTime(String fuelOnboardTime) {
        this.fuelOnboardTime = fuelOnboardTime;
    }

    /**
     * @return the personsOnboard
     */
    public int getPersonsOnboard() {
        return personsOnboard;
    }

    /**
     * @param personsOnboard the personsOnboard to set
     */
    public void setPersonsOnboard(int personsOnboard) {
        this.personsOnboard = personsOnboard;
    }

    /**
     * @return the aircraftColourMarkings
     */
    public String getAircraftColourMarkings() {
        return aircraftColourMarkings;
    }

    /**
     * @param aircraftColourMarkings the aircraftColourMarkings to set
     */
    public void setAircraftColourMarkings(String aircraftColourMarkings) {
        this.aircraftColourMarkings = aircraftColourMarkings;
    }

    /**
     * @return the pilotInCommand
     */
    public String getPilotInCommand() {
        return pilotInCommand;
    }

    /**
     * @param pilotInCommand the pilotInCommand to set
     */
    public void setPilotInCommand(String pilotInCommand) {
        this.pilotInCommand = pilotInCommand;
    }

    /**
     * @return the planStatus
     */
    public String getPlanStatus() {
        return planStatus;
    }

    /**
     * @param planStatus the planStatus to set
     */
    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }

}
