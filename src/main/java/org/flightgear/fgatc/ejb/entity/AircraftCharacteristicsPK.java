/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author scotth
 */
@Embeddable
public class AircraftCharacteristicsPK {
    @Column(length=15)
    private String aircraftType;
    @Column(length=200)
    private String characteristicKey;

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the characteristicKey
     */
    public String getCharacteristicKey() {
        return characteristicKey;
    }

    /**
     * @param characteristicKey the characteristicKey to set
     */
    public void setCharacteristicKey(String characteristicKey) {
        this.characteristicKey = characteristicKey;
    }

}
