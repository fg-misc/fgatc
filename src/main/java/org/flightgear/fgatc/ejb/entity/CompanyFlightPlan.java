/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author scotth
 */
@Entity
public class CompanyFlightPlan implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String planCode;  //this is the assigned flight plan id.
    private String airline;
    private String creatorUsername;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date   dateCreated;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date   lastUpdated;
    private String scheduledDays;  // (0=Sun, 6=Sat)
    @Temporal(javax.persistence.TemporalType.TIME)
    private Date scheduledTime;  //format HH:MM
    private String departureICAO;
    private String destinationICAO;
    private String alternateICAO;
    private String aircraftType;
    private int    flightLevel;
    private int    cruiseSpeed;
    private String totalEET; //format HHMM
    private String flightRules;
    private String flightType;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyFlightPlan)) {
            return false;
        }
        CompanyFlightPlan other = (CompanyFlightPlan) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.flightgear.fgatc.ejb.entity.CompanyFPlan[id=" + id + "]";
    }

    /**
     * @return the airline
     */
    public String getAirline() {
        return airline;
    }

    /**
     * @param airline the airline to set
     */
    public void setAirline(String airline) {
        this.airline = airline;
    }

    /**
     * @return the planCode
     */
    public String getPlanCode() {
        return planCode;
    }

    /**
     * @param planCode the planCode to set
     */
    public void setPlanCode(String planCode) {
        this.planCode = planCode;
    }

    /**
     * @return the creatorUsername
     */
    public String getCreatorUsername() {
        return creatorUsername;
    }

    /**
     * @param creatorUsername the creatorUsername to set
     */
    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    /**
     * @return the dateCreated
     */
    public Date getDateCreated() {
        return dateCreated;
    }

    /**
     * @param dateCreated the dateCreated to set
     */
    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    /**
     * @return the lastUpdated
     */
    public Date getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * @return the scheduledDays
     */
    public String getScheduledDays() {
        return scheduledDays;
    }

    /**
     * @param scheduledDays the scheduledDays to set
     */
    public void setScheduledDays(String scheduledDays) {
        this.scheduledDays = scheduledDays;
    }

    /**
     * @return the scheduledTime
     */
    public Date getScheduledTime() {
        return scheduledTime;
    }

    /**
     * @param scheduledTime the scheduledTime to set
     */
    public void setScheduledTime(Date scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    /**
     * @return the departureICAO
     */
    public String getDepartureICAO() {
        return departureICAO;
    }

    /**
     * @param departureICAO the departureICAO to set
     */
    public void setDepartureICAO(String departureICAO) {
        this.departureICAO = departureICAO;
    }

    /**
     * @return the destinationICAO
     */
    public String getDestinationICAO() {
        return destinationICAO;
    }

    /**
     * @param destinationICAO the destinationICAO to set
     */
    public void setDestinationICAO(String destinationICAO) {
        this.destinationICAO = destinationICAO;
    }

    /**
     * @return the alternateICAO
     */
    public String getAlternateICAO() {
        return alternateICAO;
    }

    /**
     * @param alternateICAO the alternateICAO to set
     */
    public void setAlternateICAO(String alternateICAO) {
        this.alternateICAO = alternateICAO;
    }

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the flightLevel
     */
    public int getFlightLevel() {
        return flightLevel;
    }

    /**
     * @param flightLevel the flightLevel to set
     */
    public void setFlightLevel(int flightLevel) {
        this.flightLevel = flightLevel;
    }

    /**
     * @return the cruiseSpeed
     */
    public int getCruiseSpeed() {
        return cruiseSpeed;
    }

    /**
     * @param cruiseSpeed the cruiseSpeed to set
     */
    public void setCruiseSpeed(int cruiseSpeed) {
        this.cruiseSpeed = cruiseSpeed;
    }

    /**
     * @return the totalEET
     */
    public String getTotalEET() {
        return totalEET;
    }

    /**
     * @param totalEET the totalEET to set
     */
    public void setTotalEET(String totalEET) {
        this.totalEET = totalEET;
    }

    /**
     * @return the flightRules
     */
    public String getFlightRules() {
        return flightRules;
    }

    /**
     * @param flightRules the flightRules to set
     */
    public void setFlightRules(String flightRules) {
        this.flightRules = flightRules;
    }

    /**
     * @return the flightType
     */
    public String getFlightType() {
        return flightType;
    }

    /**
     * @param flightType the flightType to set
     */
    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

}
