/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author scotth
 */
@Entity
public class AircraftCharacteristics implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    private AircraftCharacteristicsPK id;
    private String characteristicValue;

    public AircraftCharacteristicsPK getId() {
        return id;
    }

    public void setId(AircraftCharacteristicsPK id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AircraftCharacteristics)) {
            return false;
        }
        AircraftCharacteristics other = (AircraftCharacteristics) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.flightgear.fgatc.ejb.entity.AircraftCharacteristics[id=" + id + "]";
    }

    /**
     * @return the characteristicValue
     */
    public String getCharacteristicValue() {
        return characteristicValue;
    }

    /**
     * @param characteristicValue the characteristicValue to set
     */
    public void setCharacteristicValue(String characteristicValue) {
        this.characteristicValue = characteristicValue;
    }

}
