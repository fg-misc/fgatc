/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.ejb.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author scotth
 */
@Entity
@Table(name="MAINT_RECORD")
@NamedQueries({
    @NamedQuery(name="maintRecord.findAll", query="select m from ATNMaintenanceRecord m order by m.atnTime desc"),
    @NamedQuery(name="maintRecord.findByCallsign", query="select m from ATNMaintenanceRecord m where m.callsign = :callsign order by m.atnTime asc"),
    @NamedQuery(name="maintRecord.findTypeByCallsign", query="select m from ATNMaintenanceRecord m where m.callsign = :callsign and m.recordType = :type order by m.atnTime asc")
})
public class ATNMaintenanceRecord implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date atnTime;
    private String atnSessionId;
    private String aircraftType;
    private String atnUser;
    private String callsign;
    private String recordType;
    private String recordKey;
    private String recordValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ATNMaintenanceRecord)) {
            return false;
        }
        ATNMaintenanceRecord other = (ATNMaintenanceRecord) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.flightgear.fgatc.ejb.entity.ATNMaintenanceRecord[ id=" + id + " ]";
    }

    /**
     * @return the timestamp
     */
    public Date getAtnTime() {
        return atnTime;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setAtnTime(Date atnTime) {
        this.atnTime = atnTime;
    }

    /**
     * @return the atnSessionId
     */
    public String getAtnSessionId() {
        return atnSessionId;
    }

    /**
     * @param atnSessionId the atnSessionId to set
     */
    public void setAtnSessionId(String atnSessionId) {
        this.atnSessionId = atnSessionId;
    }

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the atnUser
     */
    public String getAtnUser() {
        return atnUser;
    }

    /**
     * @param atnUser the atnUser to set
     */
    public void setAtnUser(String atnUser) {
        this.atnUser = atnUser;
    }

    /**
     * @return the callsign
     */
    public String getCallsign() {
        return callsign;
    }

    /**
     * @param callsign the callsign to set
     */
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    /**
     * @return the recordType
     */
    public String getRecordType() {
        return recordType;
    }

    /**
     * @param recordType the recordType to set
     */
    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    /**
     * @return the recordKey
     */
    public String getRecordKey() {
        return recordKey;
    }

    /**
     * @param recordKey the recordKey to set
     */
    public void setRecordKey(String recordKey) {
        this.recordKey = recordKey;
    }

    /**
     * @return the recordValue
     */
    public String getRecordValue() {
        return recordValue;
    }

    /**
     * @param recordValue the recordValue to set
     */
    public void setRecordValue(String recordValue) {
        this.recordValue = recordValue;
    }
    
}
