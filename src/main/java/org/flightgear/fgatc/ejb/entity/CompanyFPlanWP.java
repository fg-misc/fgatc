/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;

/**
 *
 * @author scotth
 */
@Entity
public class CompanyFPlanWP implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String planCode;
    private int    seqNum;
    private String wpName;
    private String wpType;   //FIX, VOR, Airway, STAR, SID
    private String wpAction;   //eg DCT, VIA
    private int    altConstraint;  // or zero if none
    private int    spdConstraint;  // or zero if none
    


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompanyFPlanWP)) {
            return false;
        }
        CompanyFPlanWP other = (CompanyFPlanWP) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.flightgear.fgatc.ejb.entity.CompanyFPlan[id=" + id + "]";
    }


}
