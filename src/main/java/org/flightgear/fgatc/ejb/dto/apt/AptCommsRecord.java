/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.apt;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
@XmlType
public class AptCommsRecord extends AptRecord {
    public double frequencyMHz;
    public String radioType;

    /**
     * @return the frequencyMHz
     */
    @XmlElement
    public double getFrequencyMHz() {
        return frequencyMHz;
    }

    /**
     * @param frequencyMHz the frequencyMHz to set
     */
    public void setFrequencyMHz(double frequencyMHz) {
        this.frequencyMHz = frequencyMHz;
    }

    /**
     * @return the radioType
     */
    @XmlElement
    public String getRadioType() {
        return radioType;
    }

    /**
     * @param radioType the radioType to set
     */
    public void setRadioType(String radioType) {
        this.radioType = radioType;
    }
}
