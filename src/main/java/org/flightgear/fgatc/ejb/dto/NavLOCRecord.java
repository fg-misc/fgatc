/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class NavLOCRecord extends NavRecord {
    public int   elevationFeet;
    public float frequencyMhz;
    public int   minReceptionNM;
    public float localiserBearingTrue;
    public String localiserId;
    public String airportICAO;
    public String runway;
    public String localiserType;   //cat-i, cat-ii, cat-iii, LOC, LDA, SDF

    public NavLOCRecord() {
    }

}
