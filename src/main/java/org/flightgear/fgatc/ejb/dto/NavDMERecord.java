/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class NavDMERecord extends NavRecord {
    public int   elevationFeet;
    public float frequencyMhz;
    public int   minReceptionNM;
    public float dmeBiasNM;
    public String dmeId;
    public String airportICAO;
    public String runway;
    public String dmeName;
    public String dmeType;   //DME, DME-ILS, VOR-DME, VORTAC-DME etc
}
