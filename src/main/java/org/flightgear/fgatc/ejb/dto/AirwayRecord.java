/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class AirwayRecord {
    public String airwayName;
    public String intersectBegin;
    public float  beginLatDecimal;
    public float  beginLonDecimal;
    public String intersectEnd;
    public float  endLatDecimal;
    public float  endLonDecimal;
    public int    airwayType;   //1 = LOW, 2 = HIGH
    public int    baseFL;
    public int    topFL;

    /**
     * @return the airwayName
     */
    public String getAirwayName() {
        return airwayName;
    }

    /**
     * @return the intersectBegin
     */
    public String getIntersectBegin() {
        return intersectBegin;
    }

    /**
     * @return the beginLatDecimal
     */
    public float getBeginLatDecimal() {
        return beginLatDecimal;
    }

    /**
     * @return the beginLonDecimal
     */
    public float getBeginLonDecimal() {
        return beginLonDecimal;
    }

    /**
     * @return the intersectEnd
     */
    public String getIntersectEnd() {
        return intersectEnd;
    }

    /**
     * @return the endLatDecimal
     */
    public float getEndLatDecimal() {
        return endLatDecimal;
    }

    /**
     * @return the endLonDecimal
     */
    public float getEndLonDecimal() {
        return endLonDecimal;
    }

    /**
     * @return the airwayType
     */
    public int getAirwayType() {
        return airwayType;
    }

    /**
     * @return the baseFL
     */
    public int getBaseFL() {
        return baseFL;
    }

    /**
     * @return the topFL
     */
    public int getTopFL() {
        return topFL;
    }
}
