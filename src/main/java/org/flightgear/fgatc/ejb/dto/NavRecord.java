/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto;

import javax.xml.bind.annotation.*;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;

/**
 *
 * @author scotth
 */
@XmlTransient
@XmlAccessorType(XmlAccessType.NONE)
@XmlType
@XmlSeeAlso( { NavVORRecord.class, NavNDBRecord.class, NavLOCRecord.class })
public abstract class NavRecord {
    public int rowType;
    public float latDecimal;
    public float lonDecimal;

    /**
     * @return the latDecimal
     */
    @XmlElement
    public float getLatDecimal() {
        return latDecimal;
    }

    /**
     * @param latDecimal the latDecimal to set
     */
    public void setLatDecimal(float latDecimal) {
        this.latDecimal = latDecimal;
    }

    /**
     * @return the lonDecimal
     */
    @XmlElement
    public float getLonDecimal() {
        return lonDecimal;
    }

    /**
     * @param lonDecimal the lonDecimal to set
     */
    public void setLonDecimal(float lonDecimal) {
        this.lonDecimal = lonDecimal;
    }


}
