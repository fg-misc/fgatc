/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.apt;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
@XmlRootElement
@XmlType
public class AptLandRecord extends AptRecord {
    public float    elevationFt;
    public boolean  hasATC;
    public String   icaoId;
    public String   airportName;
    public List<AptRunwayRecord> runways;
    public AptTowerRecord tower;
    public List<AptCommsRecord> radio;

    /**
     * @return the elevationFt
     */
    @XmlElement
    public float getElevationFt() {
        return elevationFt;
    }

    /**
     * @param elevationFt the elevationFt to set
     */
    public void setElevationFt(float elevationFt) {
        this.elevationFt = elevationFt;
    }

    /**
     * @return the hasATC
     */
    @XmlElement
    public boolean isATC() {
        return hasATC;
    }

    /**
     * @param hasATC the hasATC to set
     */
    public void setHasATC(boolean hasATC) {
        this.hasATC = hasATC;
    }

    /**
     * @return the icaoId
     */
    @XmlElement
    public String getIcaoId() {
        return icaoId;
    }

    /**
     * @param icaoId the icaoId to set
     */
    public void setIcaoId(String icaoId) {
        this.icaoId = icaoId;
    }

    /**
     * @return the airportName
     */
    @XmlElement
    public String getAirportName() {
        return airportName;
    }

    /**
     * @param airportName the airportName to set
     */
    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    /**
     * @return the runways
     */
    @XmlElement
    public List<AptRunwayRecord> getRunways() {
        return runways;
    }

    /**
     * @param runways the runways to set
     */
    public void setRunways(List<AptRunwayRecord> runways) {
        this.runways = runways;
    }

    /**
     * @return the tower
     */
    @XmlElement
    public AptTowerRecord getTower() {
        return tower;
    }

    /**
     * @param tower the tower to set
     */
    public void setTower(AptTowerRecord tower) {
        this.tower = tower;
    }

    /**
     * @return the radio
     */
    @XmlElement
    public List<AptCommsRecord> getRadio() {
        return radio;
    }

    /**
     * @param radio the radio to set
     */
    public void setRadio(List<AptCommsRecord> radio) {
        this.radio = radio;
    }

}
