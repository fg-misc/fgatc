/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class NavBeaconRecord extends NavRecord {
    public int   elevationFeet;
    public float localiserBearingTrue;
    public String airportICAO;
    public String runway;
    public String beaconType;   //OM, MM, IM



}
