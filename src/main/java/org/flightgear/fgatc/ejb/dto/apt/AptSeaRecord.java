/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.apt;

import javax.xml.bind.annotation.XmlRootElement;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
@XmlRootElement
public class AptSeaRecord extends AptRecord {
    public String icaoId;

    /**
     * @return the icaoId
     */
    public String getIcaoId() {
        return icaoId;
    }

    /**
     * @param icaoId the icaoId to set
     */
    public void setIcaoId(String icaoId) {
        this.icaoId = icaoId;
    }

}
