/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.nav;

import javax.xml.bind.annotation.XmlType;
import org.flightgear.fgatc.ejb.dto.NavRecord;

/**
 *
 * @author scotth
 */
@XmlType
public class NavLOCRecord extends NavRecord {
    public int   elevationFeet;
    public float frequencyMhz;
    public int   minReceptionNM;
    public float localiserBearingTrue;
    public String localiserId;
    public String airportICAO;
    public String runway;
    public String localiserType;   //cat-i, cat-ii, cat-iii, LOC, LDA, SDF

    public NavLOCRecord() {
    }

}
