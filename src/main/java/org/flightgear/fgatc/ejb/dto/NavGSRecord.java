/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class NavGSRecord extends NavRecord {
    public int    elevationFeet;
    public float  frequencyMhz;
    public int    minReceptionNM;
    public float  localiserBearingTrue;
    public float  glideslopeAngle;
    public String localiserId;
    public String airportICAO;
    public String runway;
    public String localiserType;   //cat-i, cat-ii, cat-iii, LOC, LDA, SDF

}
