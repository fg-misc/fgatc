/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.apt;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
@XmlType
public class AptRunwayRecord extends AptRecord {
    public String runwayId;
    public float  runwayLat;
    public float  runwayLon;
    public float  runwayHeadingDeg;
    public float  runwayWidthMetres;
    public int  runwayLengthMetres;
    public int  runwayLengthFeet;
    public int  runwayOverrunLengthMetres;
    public int  runwayOverrunLengthFeet;

    /**
     * @return the runwayId
     */
    @XmlElement
    public String getRunwayId() {
        return runwayId;
    }

    /**
     * @param runwayId the runwayId to set
     */
    public void setRunwayId(String runwayId) {
        this.runwayId = runwayId;
    }

    /**
     * @return the runwayLat
     */
    @XmlElement
    public float getRunwayLat() {
        return runwayLat;
    }

    /**
     * @param runwayLat the runwayLat to set
     */
    public void setRunwayLat(float runwayLat) {
        this.runwayLat = runwayLat;
    }

    /**
     * @return the runwayLon
     */
    @XmlElement
    public float getRunwayLon() {
        return runwayLon;
    }

    /**
     * @param runwayLon the runwayLon to set
     */
    public void setRunwayLon(float runwayLon) {
        this.runwayLon = runwayLon;
    }

    /**
     * @return the runwayHeadingDeg
     */
    @XmlElement
    public float getRunwayHeadingDeg() {
        return runwayHeadingDeg;
    }

    /**
     * @param runwayHeadingDeg the runwayHeadingDeg to set
     */
    public void setRunwayHeadingDeg(float runwayHeadingDeg) {
        this.runwayHeadingDeg = runwayHeadingDeg;
    }

    /**
     * @return the runwayWidthMetres
     */
    @XmlElement
    public float getRunwayWidthMetres() {
        return runwayWidthMetres;
    }

    /**
     * @param runwayWidthMetres the runwayWidthMetres to set
     */
    public void setRunwayWidthMetres(float runwayWidthMetres) {
        this.runwayWidthMetres = runwayWidthMetres;
    }

    /**
     * @return the runwayLengthMetres
     */
    @XmlElement
    public int getRunwayLengthMetres() {
        return runwayLengthMetres;
    }

    /**
     * @param runwayLengthMetres the runwayLengthMetres to set
     */
    public void setRunwayLengthMetres(int runwayLengthMetres) {
        this.runwayLengthMetres = runwayLengthMetres;
    }

    /**
     * @return the runwayLengthFeet
     */
    @XmlElement
    public int getRunwayLengthFeet() {
        return runwayLengthFeet;
    }

    /**
     * @param runwayLengthFeet the runwayLengthFeet to set
     */
    public void setRunwayLengthFeet(int runwayLengthFeet) {
        this.runwayLengthFeet = runwayLengthFeet;
    }

    /**
     * @return the runwayOverrunLengthMetres
     */
    @XmlElement
    public int getRunwayOverrunLengthMetres() {
        return runwayOverrunLengthMetres;
    }

    /**
     * @param runwayOverrunLengthMetres the runwayOverrunLengthMetres to set
     */
    public void setRunwayOverrunLengthMetres(int runwayOverrunLengthMetres) {
        this.runwayOverrunLengthMetres = runwayOverrunLengthMetres;
    }

    /**
     * @return the runwayOverrunLengthFeet
     */
    @XmlElement
    public int getRunwayOverrunLengthFeet() {
        return runwayOverrunLengthFeet;
    }

    /**
     * @param runwayOverrunLengthFeet the runwayOverrunLengthFeet to set
     */
    public void setRunwayOverrunLengthFeet(int runwayOverrunLengthFeet) {
        this.runwayOverrunLengthFeet = runwayOverrunLengthFeet;
    }

}
