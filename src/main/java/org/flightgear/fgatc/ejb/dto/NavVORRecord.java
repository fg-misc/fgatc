/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.flightgear.fgatc.ejb.dto;

/**
 *
 * @author scotth
 */
public class NavVORRecord extends NavRecord {
    public int   elevationFeet;
    public float frequencyMhz;
    public int   minReceptionNM;
    public float slaveVariation;
    public String vorId;
    public String vorName;

}
