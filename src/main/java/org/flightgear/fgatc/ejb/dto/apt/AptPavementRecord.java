/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb.dto.apt;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
@XmlType
public class AptPavementRecord extends AptRecord {
    public String pavementId;
    public float  pavementLat;
    public float  pavementLon;
    public float  pavementHeadingDeg;
    public int    pavementSurfaceType;

    /**
     * @return the pavementId
     */
    public String getPavementId() {
        return pavementId;
    }

    /**
     * @param pavementId the pavementId to set
     */
    public void setPavementId(String pavementId) {
        this.pavementId = pavementId;
    }

    /**
     * @return the pavementLat
     */
    public float getPavementLat() {
        return pavementLat;
    }

    /**
     * @param pavementLat the pavementLat to set
     */
    public void setPavementLat(float pavementLat) {
        this.pavementLat = pavementLat;
    }

    /**
     * @return the pavementLon
     */
    public float getPavementLon() {
        return pavementLon;
    }

    /**
     * @param pavementLon the pavementLon to set
     */
    public void setPavementLon(float pavementLon) {
        this.pavementLon = pavementLon;
    }

    /**
     * @return the pavementHeadingDeg
     */
    public float getPavementHeadingDeg() {
        return pavementHeadingDeg;
    }

    /**
     * @param pavementHeadingDeg the pavementHeadingDeg to set
     */
    public void setPavementHeadingDeg(float pavementHeadingDeg) {
        this.pavementHeadingDeg = pavementHeadingDeg;
    }

    /**
     * @return the pavementSurfaceType
     */
    public int getPavementSurfaceType() {
        return pavementSurfaceType;
    }

    /**
     * @param pavementSurfaceType the pavementSurfaceType to set
     */
    public void setPavementSurfaceType(int pavementSurfaceType) {
        this.pavementSurfaceType = pavementSurfaceType;
    }


    

}
