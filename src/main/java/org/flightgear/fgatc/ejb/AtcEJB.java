/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.ejb;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import org.flightgear.fgatc.AtnCommandMessage;
import org.flightgear.fgatc.AtnRequest;
import org.flightgear.fgatc.AtnSession;
import org.flightgear.fgatc.Geo;
import org.flightgear.fgatc.LatLon;
import org.flightgear.fgatc.MultiPlayer;
import org.flightgear.fgatc.NoStationFoundException;
import org.flightgear.fgatc.Station;
import org.flightgear.fgatc.ejb.dto.AirwayRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptCommsRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptRunwayRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptTowerRecord;

/**
 * Abstract: Provides transactional singleton access to ATC stations and the MP
 * status list
 *
 * @author scotth
 */
@Singleton
public class AtcEJB {

    @EJB
    private NavEJB navEJB;
    @EJB
    private AtnSessionHolderEJB atnGlobal;
    private List<Station> activeStations = new LinkedList();
    private Map multiUsers = Collections.synchronizedMap(new HashMap());
    //private HashMap multiUsers = new HashMap();
    private Map atcRequests = Collections.synchronizedMap(new HashMap());
    private boolean getMP = true;
    private int autoCount = 0;
    private int nextRequestSeq = 123;

    public AtcEJB() {
    }

    /**
     * every 10 seconds we poll the MP server and update our Map of players
     *
     *
     */
    @Schedule(second = "*/10", minute = "*", hour = "*", persistent = false)
    public void doMultiplayerUpdate() {
        MultiplayerParser parser = new MultiplayerParser();
        Logger log = Logger.getLogger(this.getClass().getName() + ".doMultiplayerUpdate");
        if (getMP == true) {
            try {
                List mpList = parser.parse("mpserver04.flightgear.org");
                // make a hashmap of new users to make the stale code work better
                HashMap mpHash = new HashMap();
                for (Iterator m = mpList.iterator(); m.hasNext();) {
                    MultiPlayer usr = (MultiPlayer) m.next();
                    mpHash.put(usr.callsign, usr);
                }
                // first remove stale users
                Set usrList = multiUsers.keySet();
                synchronized (multiUsers) {
                    for (Iterator p = usrList.iterator(); p.hasNext();) {
                        String key = (String) p.next();
                        if (mpHash.containsKey(key) == false) {
                            MultiPlayer tmp = (MultiPlayer) multiUsers.get(key);
                            tmp.staleTime++;
                            if (tmp.staleTime > 5) {
                                p.remove();
                                log.fine("Removing MP: " + key);
                            } else {
                                // else we put it back with updated stale time
                                multiUsers.put(key, tmp);
                                log.finer("Increment MP stale: " + key);
                            }
                        }
                    }
                }
                // now update remaining users
                for (Iterator m = mpList.iterator(); m.hasNext();) {
                    MultiPlayer player = (MultiPlayer) m.next();
                    player.staleTime = 0;
                    if (multiUsers.containsKey(player.callsign)) {
                        MultiPlayer prevCycle = (MultiPlayer) multiUsers.get(player.callsign);
                        //calculate speed and heading from previous place
                        for (Iterator s = activeStations.iterator(); s.hasNext();) {
                            Station station = (Station) s.next();
                            if (station.isActive() == false) {
                                List aptList = navEJB.findMatchingICAO(station.getAirportIcaoId());
                                for (Iterator a = aptList.iterator(); a.hasNext();) {
                                    AptLandRecord apt = (AptLandRecord) a.next();
                                    if (apt.hasATC == true) {
                                        AptTowerRecord atc = apt.tower;
                                        // make a bounding box of some size around the ATC tower incase of variances between position of tower
                                        float latL = (float) (atc.towerLat - 0.035);
                                        float latR = (float) (atc.towerLat + 0.035);
                                        float lonB = (float) (atc.towerLon - 0.035);
                                        float lonT = (float) (atc.towerLon + 0.035);
                                        if (player.lat > latL && player.lat < latR && player.lon < lonT && player.lon > lonB) {
                                            //player.callsign.toUpperCase().contains(station.getCallsign().toUpperCase()) &&  
                                            log.fine("player callsign: " + player.callsign + " is within area of station: " + station.getAirportIcaoId() + " with type: " + player.aircraftType);
                                            if (player.aircraftType.toLowerCase().matches(".*(atc|openradar).*") == true) {
                                                station.setActive(true);
                                            }
                                        } else {
                                            log.fine("player callsign: " + player.callsign + " lat: " + player.lat + ", lon: " + player.lon + " NOT within latL: " + latL + ", latR: " + latR + ", lonT: " + lonT + ", lonB: " + lonB);
                                        }
                                    }
                                }
                            }
                        }

                    } else {
                        // about to do fresh insert
                    }
                    multiUsers.put(player.callsign, player);
                }
            } catch (Exception ex) {
                //ignore...
            }
        }
        performAutomatedATCRequests();
    }

    /**
     * Add a ATC station to the active list
     *
     * @param item
     */
    public void addNewStation(Station item) {
        this.activeStations.add(item);
    }

    /**
     * Find a ATC station by username
     *
     * @param username
     * @return
     * @throws NoStationFoundException
     */
    public Station getStationByUser(String username) throws NoStationFoundException {
        for (Station s : activeStations) {
            if (s.getUsername().equalsIgnoreCase(username)) {
                return s;
            }
        }
        throw new NoStationFoundException("No station for " + username);
    }

    /**
     * Return all ATC stations
     *
     * @return
     */
    public List<Station> findAllActiveStations() {
        List retList = new ArrayList();
        for (Iterator s = activeStations.iterator(); s.hasNext();) {
            Station stat = (Station) s.next();
            if (stat.isActive() == true) {
                retList.add(stat);
            }
        }
        return retList;
    }

    /**
     * finds all matching ATC stations that are at a ICAO location
     *
     * @param icao
     * @return
     */
    public List<Station> findActiveStationByICAO(String icao) {
        List retList = new ArrayList();
        for (Iterator s = activeStations.iterator(); s.hasNext();) {
            Station stat = (Station) s.next();
            if (stat.getAirportIcaoId().equalsIgnoreCase(icao)) {
                retList.add(stat);
            }
        }
        return retList;
    }

    /**
     * remove ATC station from list so it is no longer active
     *
     * @param item
     */
    public void removeStation(Station item) {
        activeStations.remove(item);
    }

    /**
     * stops the network fetching of MP status
     */
    public void stopMPFetch() {
        this.getMP = false;
    }

    /**
     * enables the network fetching of MP statuses
     */
    public void startMPFetch() {
        this.getMP = true;
    }

    /**
     *
     * @return
     */
    public List<MultiPlayer> getAllMultiplayers() {
        List retList = new ArrayList();
        retList.addAll(this.multiUsers.values());
        return retList;
    }

    private void performAutomatedATCRequests() {
        Set kSet = this.atcRequests.keySet();
        for (Iterator k = this.atcRequests.values().iterator(); k.hasNext();) {
            AtnRequest req = (AtnRequest)k.next();
            if (req.requestType.equalsIgnoreCase("DEPART") && req.active == true) {
                LatLon[] box = Geo.calculateBoundingBoxByDistanceKm(req.session.posLat, req.session.posLon, 0.1);
                boolean approved = true;
                for (Iterator m = this.multiUsers.values().iterator(); m.hasNext();) {
                    MultiPlayer mp = (MultiPlayer) m.next();
                    if (mp.lat > box[1].lat && mp.lat < box[0].lat && mp.lon < box[0].lon && mp.lon > box[1].lon) {
                        approved = false;
                    }
                }
                if (approved == true) {
                    req.setStatus("APPROVED");
                    AtnSession sess = atnGlobal.findSession(req.session.sessionId);
                    AtnCommandMessage msg = new AtnCommandMessage();
                    msg.command = "MAILBOX";
                    msg.commandMessage = "DEPART CLEARANCE " + req.getStatus();
                    msg.commandAirport = req.requestParams.get("depart");
                    sess.getCmdQueue().add(msg);
                    atnGlobal.addSession(sess);
                    req.active = false;
                } else {
                    if (req.getStatus().equalsIgnoreCase("REQUESTED")) {
                        req.setStatus("RECEIVED");
                        AtnSession sess = atnGlobal.findSession(req.session.sessionId);
                        AtnCommandMessage msg = new AtnCommandMessage();
                        msg.command = "MAILBOX";
                        msg.commandMessage = "DEPART CLEARANCE " + req.getStatus();
                        msg.commandAirport = req.requestParams.get("depart");
                        sess.getCmdQueue().add(msg);
                        atnGlobal.addSession(sess);
                    } else {
                        req.setStatus("REJECTED");
                        AtnSession sess = atnGlobal.findSession(req.session.sessionId);
                        AtnCommandMessage msg = new AtnCommandMessage();
                        msg.command = "MAILBOX";
                        msg.commandMessage = "DEPART CLEARANCE " + req.getStatus();
                        msg.commandAirport = req.requestParams.get("depart");
                        sess.getCmdQueue().add(msg);
                        atnGlobal.addSession(sess);
                        req.active = false;
                    }
                }
            }
        }
    }

    public void addATCRequest(AtnRequest request) {
        if (request.id == null) {
            request.id = String.valueOf(nextRequestSeq++);
        }
        this.atcRequests.put(request.id, request);
    }
}
