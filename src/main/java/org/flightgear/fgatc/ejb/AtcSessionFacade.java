/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb;

import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.flightgear.fgatc.ejb.entity.ATNMaintenanceRecord;
import org.flightgear.fgatc.ejb.entity.FlightPlan;
import org.flightgear.fgatc.ejb.entity.User;

/**
 * Abstract:
 *   Provides transactional methods for access data in our database.
 *
 * @author scotth
 */
@Stateless
@LocalBean
public class AtcSessionFacade {
    @PersistenceContext(unitName="FGATC-ejbPU")
    private EntityManager em;

    /**
     * Save a JPA entity object
     *
     * @param object
     */
    public void persist(Object object) {
        em.persist(object);
    }


    /**
     * find a User entity that exactly matches the supplied username
     *
     * @param username
     * @return
     */
    public User findUser(String username) {
        return em.find(User.class, username);
    }


    /**
     * Find all Flight Plans for the current date at a ICAO code.
     *
     * @param icaoId
     * @param maxResults
     * @return
     */
    public List<FlightPlan> getActiveFlightPlans(String icaoId, int maxResults) {
        Query query = em.createNamedQuery("FlightPlan.findCurrentByICAO");
        query.setParameter("icao", icaoId);
        Date currentTime = new Date();
        query.setParameter("date", currentTime);

        if (maxResults > 0) {
            query.setMaxResults(maxResults);
        }
        return query.getResultList();
    }


    /**
     * Return all the Flight Plans for a named user.
     * 
     * @param username
     * @return
     */
    public List<FlightPlan> findUserPlans(String username) {
        Query query = em.createNamedQuery("FlightPlan.findAllByUser");
        query.setParameter("user", username);
        return query.getResultList();
    }

    public void saveMaintRecord(ATNMaintenanceRecord item) {
        item.setAtnTime(new Date());
        em.persist(item);
    }
 
}
