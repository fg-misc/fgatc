/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb;

import org.flightgear.fgatc.ejb.dto.nav.NavLOCRecord;
import org.flightgear.fgatc.ejb.dto.nav.NavNDBRecord;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.flightgear.fgatc.ejb.dto.nav.NavBeaconRecord;
import org.flightgear.fgatc.ejb.dto.nav.NavDMERecord;
import org.flightgear.fgatc.ejb.dto.nav.NavGSRecord;
import org.flightgear.fgatc.ejb.dto.NavRecord;
import org.flightgear.fgatc.ejb.dto.nav.NavVORRecord;

/**
 *
 * @author scotth
 */
public class NavParser {


    /* CONSTANTS */
    public static final int PARSER_VERSION = 810;
    public static final int ROW_NDB = 2;
    public static final int ROW_VOR = 3;
    public static final int ROW_ILS_LOC = 4;
    public static final int ROW_LOC_ONLY = 5;
    public static final int ROW_ILS_GS = 6;
    public static final int ROW_OM = 7;
    public static final int ROW_MM = 8;
    public static final int ROW_IM = 9;
    public static final int ROW_ILS_DME = 12;
    public static final int ROW_DME = 13;

    public List<NavRecord> parse(InputStream fin) {
        ArrayList retList = new ArrayList();
        Logger log = Logger.getLogger(this.getClass().getName() + ".parse()");
        log.info("Parsing nav.dat file");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.length() > 0) {
                    log.fine("readline: " + line);
                    String[] initParts = line.split("[ \t]");
                    ArrayList tmpList = new ArrayList(Arrays.asList(initParts));
                    for(Iterator p = tmpList.iterator(); p.hasNext();){
                        String part = (String)p.next();
                        if (part.length() == 0) {
                            p.remove();
                        } else {
                            log.finer("part: >>>>" + part + "<<<<");
                        }
                    }
                    String[] parts = (String[])tmpList.toArray(new String[tmpList.size()]);
                    log.fine(("parts[0] >>>"+parts[0]+"<<<"));
                    int rowType = Integer.valueOf(parts[0]).intValue();
                    NavRecord record = null;
                    switch (rowType) {
                        case ROW_NDB:
                            record = parseNDB(parts);
                            break;
                        case ROW_VOR:
                            record = parseVOR(parts);
                            break;
                        case ROW_ILS_LOC:
                            record = parseLOC(parts);
                            break;
                        case ROW_LOC_ONLY:
                            record = parseLOC(parts);
                            break;
                        case ROW_ILS_GS:
                            //record = parseGS(parts);
                            break;
                        case ROW_OM:
                            record = parseBeacon(parts);
                            break;
                        case ROW_MM:
                            record = parseBeacon(parts);
                            break;
                        case ROW_IM:
                            record = parseBeacon(parts);
                            break;
                        case ROW_ILS_DME:
                            record = parseDME(parts);
                            break;
                        case ROW_DME:
                            record = parseDME(parts);
                            break;
                    }
                    if (record != null) {
                        retList.add(record);
                    }
                }
            }
            log.info("Completed parsing of "+retList.size()+" records");
        } catch (IOException ex) {
            Logger.getLogger(NavParser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NumberFormatException nfe) {
            Logger.getLogger(NavParser.class.getName()).log(Level.SEVERE, null, nfe);
        }
        return retList;
    }

    private NavRecord parseNDB(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseNDB()");
        NavNDBRecord record = new NavNDBRecord();
        log.fine(("parts[0] >>>"+parts[0]+"<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();

        log.fine(("parts[1] >>>"+parts[1]+"<<<"));
        record.latDecimal = Float.valueOf(parts[1]).floatValue();

        log.fine(("parts[2] >>>"+parts[2]+"<<<"));
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();

        log.fine(("parts[4] >>>"+parts[4]+"<<<"));
        record.frequencykHz = Integer.valueOf(parts[4]).intValue();

        log.fine(("parts[5] >>>"+parts[5]+"<<<"));
        record.minReceptionNM = Integer.valueOf(parts[5]).intValue();

        log.fine(("parts[6] >>>"+parts[6]+"<<<"));
        record.ndbId = parts[6];

        log.fine(("parts[7] >>>"+parts[7]+"<<<"));
        record.ndbName = concat(parts, 7, parts.length);

        return record;
    }

    private NavRecord parseVOR(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseVOR()");
        NavVORRecord record = new NavVORRecord();
        log.fine(("parts[0] >>>"+parts[0]+"<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();

        log.fine(("parts[1] >>>"+parts[1]+"<<<"));
        record.latDecimal = Float.valueOf(parts[1]).floatValue();

        log.fine(("parts[2] >>>"+parts[2]+"<<<"));
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();

        log.fine(("parts[3] >>>"+parts[3]+"<<<"));
        record.elevationFeet = Integer.valueOf(parts[3]).intValue();

        log.fine(("parts[4] >>>"+parts[4]+"<<<"));
        record.frequencyMhz = (Float.valueOf(parts[4]).floatValue()) / 1000;

        log.fine(("parts[5] >>>"+parts[5]+"<<<"));
        record.minReceptionNM = Integer.valueOf(parts[5]).intValue();

        log.fine(("parts[6] >>>"+parts[6]+"<<<"));
        record.vorId = parts[6];

        log.fine(("parts[7] >>>"+parts[7]+"<<<"));
        record.vorName = concat(parts,7, parts.length);

        return record;
    }

    private NavRecord parseLOC(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseLOC()");
        NavLOCRecord record = new NavLOCRecord();
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.latDecimal = Float.valueOf(parts[1]).floatValue();
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();
        record.elevationFeet = Integer.valueOf(parts[3]).intValue();
        record.frequencyMhz = (Float.valueOf(parts[4]).floatValue()) / 1000;
        record.minReceptionNM = Integer.valueOf(parts[5]).intValue();
        record.localiserBearingTrue = Float.valueOf(parts[6]).floatValue();
        record.localiserId = parts[7];
        record.airportICAO = parts[8];
        record.runway = parts[9];
        record.localiserType = parts[10];

        return record;
    }

    private NavRecord parseGS(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseGS()");
        NavGSRecord record = new NavGSRecord();
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.latDecimal = Float.valueOf(parts[1]).floatValue();
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();
        record.elevationFeet = Integer.valueOf(parts[3]).intValue();
        record.frequencyMhz = (Float.valueOf(parts[4]).floatValue()) / 1000;
        record.minReceptionNM = Double.valueOf(parts[5]).intValue();
        String angleStr = parts[6].substring(0, 3);
        String bearingStr = parts[6].substring(3);
        record.localiserBearingTrue = Float.valueOf(bearingStr).floatValue();
        record.glideslopeAngle = (Float.valueOf(bearingStr).floatValue()) / 100;
        record.localiserId = parts[7];
        record.airportICAO = parts[8];
        record.runway = parts[9];
        record.localiserType = parts[10];
        return record;
    }

    private NavRecord parseBeacon(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseBeacon()");
        NavBeaconRecord record = new NavBeaconRecord();
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.latDecimal = Float.valueOf(parts[1]).floatValue();
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();
        record.elevationFeet = Integer.valueOf(parts[3]).intValue();
        record.localiserBearingTrue = Float.valueOf(parts[6]).floatValue();
        record.airportICAO = parts[8];
        record.runway = parts[9];
        record.beaconType = parts[10];
        return record;
    }

    private NavRecord parseDME(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseDME()");
        NavDMERecord record = new NavDMERecord();
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.latDecimal = Float.valueOf(parts[1]).floatValue();
        record.lonDecimal = Float.valueOf(parts[2]).floatValue();
        record.elevationFeet = Integer.valueOf(parts[3]).intValue();
        record.frequencyMhz = (Float.valueOf(parts[4]).floatValue()) / 1000;
        record.minReceptionNM = Integer.valueOf(parts[5]).intValue();
        record.dmeBiasNM = Float.valueOf(parts[6]).floatValue();
        record.dmeId = parts[7];
        record.dmeName = concat(parts, 8, (parts.length-2));
        //record.airportICAO = parts[8];
        //record.runway = parts[9];
        record.dmeType = parts[parts.length-1];
        return record;
    }

    private String concat(String[] parts, int start, int end) {
        StringBuffer retStr = new StringBuffer();
        for(int p = start; p != end; p++) {
            if (p > start) {
                retStr.append(" ");
            }
            retStr.append(parts[p]);
        }
        return retStr.toString();
    }
}
