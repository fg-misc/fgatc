/*
 *  Copyright (C) 2010 S.Hamilton
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.flightgear.fgatc.MultiPlayer;

/**
 *
 * @author S.Hamilton
 */
public class MultiplayerParser {

    public MultiplayerParser() {
    }

    public List parse(String server) {
        Logger log = Logger.getLogger(this.getClass().getName() + "parse");
        List retList = new ArrayList();
        Socket sock = new Socket();
        try {
            int port = 5001;
            SocketAddress addr = new java.net.InetSocketAddress(server, port);
            log.info("Connecting to " + server + ":" + port);
            sock.connect(addr, 3000);
            BufferedReader fin = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            String line = null;
            while ((line = fin.readLine()) != null) {
                if (line.startsWith("#") == false) {
                    String[] parts = line.split("[ \t\n\f\r]");
                    MultiPlayer player = new MultiPlayer();
                    for (int i = 0; i != parts.length; i++) {
                        log.fine("parts[" + i + "] >>>" + parts[i] + "<<<");
                    }
                    String[] callSignParts = parts[0].split("@");
                    if (parts.length > 6) {
                        if (callSignParts.length > 1) {
                            player.callsign = callSignParts[0];
                            player.atHost = callSignParts[1];
                            if (parts[4].equals(".") == false && parts[5].equals(".") == false) {
                                try {
                                    player.lat = Float.valueOf(parts[4]).floatValue();
                                    player.lon = Float.valueOf(parts[5]).floatValue();
                                    player.elevation = Double.valueOf(parts[6]).longValue();
                                    String aircraftStr = parts[10].substring(parts[10].lastIndexOf("/") + 1);
                                    log.finer("aircraft type1: " + aircraftStr);
                                    int pos = aircraftStr.indexOf(".xml");
                                    if (pos > 0) {
                                        aircraftStr = aircraftStr.substring(0, pos);
                                    }
                                    log.finer("aircraft type2: " + aircraftStr);
                                    player.aircraftType = aircraftStr;
                                    //player.elevation = Long.valueOf(parts[6]).longValue();
                                    retList.add(player);
                                } catch (NumberFormatException nfe) {
                                    //ignore
                                }
                            }
                        }
                    }
                }
            }
            fin.close();
            sock.close();
        } catch (IOException ex) {
            log.log(Level.SEVERE, null, ex);
            if (sock != null && sock.isClosed() == false) {
                try {
                    sock.close();
                } catch (IOException ex1) {
                    log.log(Level.SEVERE, null, ex1);
                }
            }
        }
        return retList;
    }
}
