/*
 *  Copyright (C) 2012 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import org.flightgear.fgatc.ejb.dto.AptRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptRunwayRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptSeaRecord;

/**
 * The X-Plane (Robin Peel) Airport parser.
 *
 * @author scotth
 */
public class Apt1000Parser extends AptParser {


            /* Constants */
    public static final int PARSER_VERSION = 1000;
    public static final int ROW_LAND_AIRPORT_HEADER = 1;
    public static final int ROW_SEAPLANE_AIRPORT_HEADER = 16;
    public static final int ROW_HELIPORT_AIRPORT_HEADER = 17;
    public static final int ROW_RUNWAY   = 100;
    public static final int ROW_WATER_RUNWAY  = 101;
    public static final int ROW_HELIPAD = 102;
    public static final int ROW_PAVEMENT_HEADER    = 110;
    public static final int ROW_LINEAR_FEATURE_HEADER = 120;
    public static final int ROW_AIRPORT_BOUNDARY_HEADER    = 130;
    public static final int ROW_NODE  = 111;
    public static final int ROW_BEZIER_NODE  = 112;
    public static final int ROW_NODE_IMPLICIT_LOOP  = 113;
    public static final int ROW_BEZIER_NODE_IMPLICIT_LOOP  = 114;
    public static final int ROW_NODE_TERMINATING  = 115;
    public static final int ROW_NODE_BEZIER_TERMINATING  = 116;
    public static final int ROW_AIRPORT_VIEWPOINT  = 14;
    public static final int ROW_AEROPLANE_STARTUP  = 15;
    public static final int ROW_AIRPORT_LIGHT_BEACON  = 18;
    public static final int ROW_WINDSOCK  = 19;
    public static final int ROW_TAXIWAY_SIGN  = 20;
    public static final int ROW_LIGHTING_OBJECT  = 21;
    public static final int ROW_COMMUNICATION1  = 50;
    public static final int ROW_COMMUNICATION2  = 51;
    public static final int ROW_COMMUNICATION3  = 52;
    public static final int ROW_COMMUNICATION4  = 53;
    public static final int ROW_COMMUNICATION5  = 54;
    public static final int ROW_COMMUNICATION6  = 55;
    public static final int ROW_COMMUNICATION7  = 56;


    public HashMap<String, AptRecord> parse(InputStream fin) {
        HashMap retList = new HashMap();
        Logger log = Logger.getLogger(this.getClass().getName() + ".parse()");
        log.info("Parsing apt.dat file");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.
           
            while (reader.ready()) {
                String line = reader.readLine();
                if (line.length() > 0) {
                    log.fine("readline: " + line);
                    String[] initParts = line.split("[ \t]");
                    ArrayList tmpList = new ArrayList(Arrays.asList(initParts));
                    for(Iterator p = tmpList.iterator(); p.hasNext();){
                        String part = (String)p.next();
                        if (part.length() == 0) {
                            p.remove();
                        } else {
                            log.finer("part: >>>>" + part + "<<<<");
                        }
                    }
                    String[] parts = (String[])tmpList.toArray(new String[tmpList.size()]);
                    log.fine(("parts[0] >>>"+parts[0]+"<<<"));
                    int rowType = Integer.valueOf(parts[0]).intValue();
                    AptRecord airportRecord = null;
                    AptRunwayRecord runwayRecord = null;
                    switch (rowType) {
                        case ROW_LAND_AIRPORT_HEADER:
                            airportRecord = parseAirport(parts);
                            retList.put(((AptLandRecord)airportRecord).icaoId, airportRecord);
                            break;
                        case ROW_SEAPLANE_AIRPORT_HEADER:
                            airportRecord = parseSeaAirport(parts);
                            retList.put(((AptLandRecord)airportRecord).icaoId, airportRecord);
                            break;
                        case ROW_RUNWAY:
                            runwayRecord = parseRunway(parts);
                            if (airportRecord.rowType == ROW_LAND_AIRPORT_HEADER) {
                                ((AptLandRecord)airportRecord).runways.add(runwayRecord);
                            }
                            break;

                    }
                }
            }
        }catch(IOException ioe) {
            //
        }
        log.info("Completed parsing of "+retList.size()+" records");
        return retList;
    }

    private AptRecord parseAirport(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseLand()");
        AptLandRecord record = new AptLandRecord();
        log.fine(("parts[0] >>>"+parts[0]+"<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();
        record.elevationFt = Float.valueOf(parts[1]).floatValue();
        record.hasATC = Boolean.valueOf(parts[2]).booleanValue();
        record.icaoId = parts[4];
        record.airportName = super.concat(parts, 5, parts.length);

        return record;
    }

    private AptRecord parseSeaAirport(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseSeaAirport()");
        AptSeaRecord record = new AptSeaRecord();
        log.fine(("parts[0] >>>"+parts[0]+"<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();

        return record;
    }

    private AptRunwayRecord parseRunway(String[] parts) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".parseRunway()");
        AptRunwayRecord record = new AptRunwayRecord();
        log.fine(("parts[0] >>>"+parts[0]+"<<<"));
        record.rowType = Integer.valueOf(parts[0]).intValue();

        return record;
    }


}
