/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.ejb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipFile;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import org.flightgear.fgatc.Geo;
import org.flightgear.fgatc.LatLon;
import org.flightgear.fgatc.ejb.dto.AirwayRecord;
import org.flightgear.fgatc.ejb.dto.AptRecord;
import org.flightgear.fgatc.ejb.dto.FixRecord;
import org.flightgear.fgatc.ejb.dto.NavRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;

/**
 * Abstract: A transactional singleton to hold the Navaids, Airport and Airways
 * data parsed on startup from the FlightGear data files.
 *
 * @author scotth
 */
@Singleton
@Startup
public class NavEJB {

    private HashMap Airports = new HashMap();
    private String fgRoot;
    private List<NavRecord> navRecords;
    private HashMap<String, AptRecord> aptRecords;
    private HashMap<String, List<AirwayRecord>> awyRecords;

    public NavEJB() {
        fgRoot = System.getenv("FG_ROOT");
        if (fgRoot == null) {
            fgRoot = "/u01/app/FlightGear-2.0/share/FlightGear/data";
        }
    }

    @PostConstruct
    public void init() {
        Logger.getLogger(NavEJB.class.getName()).log(Level.INFO, null, "Initialising databases in " + fgRoot);
        initNavFile();
        initAptFile();
        initAwyFile();
    }

    /**
     * Find nav.dat or nav.dat.gz file and parse its contents. This is called
     * only once on startup.
     *
     */
    private void initNavFile() {
        File file = new File(fgRoot + "/Navaids/nav.dat.gz");
        InputStream fin = null;
        if (file.exists() == false) {
            file = new File(fgRoot + "/Navaids/nav.dat");
            try {
                fin = new java.io.FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                fin = new GZIPInputStream(new java.io.FileInputStream(file));
            } catch (IOException ex) {
                Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        NavParser parser = new NavParser();
        navRecords = parser.parse(fin);
        try {
            fin.close();
        } catch (IOException ex) {
            Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Find nav.dat or nav.dat.gz file and parse its contents. This is called
     * only once on startup.
     *
     */
    private void initAwyFile() {
        File file = new File(fgRoot + "/Navaids/awy.dat.gz");
        InputStream fin = null;
        if (file.exists() == false) {
            file = new File(fgRoot + "/Navaids/awy.dat");
            try {
                fin = new java.io.FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                fin = new GZIPInputStream(new java.io.FileInputStream(file));
            } catch (IOException ex) {
                Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        AwyParser parser = new AwyParser();
        this.awyRecords = parser.parse(fin);
        try {
            fin.close();
        } catch (IOException ex) {
            Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Find apt.dat or apt.dat.gz file and parse its contents. This is called
     * only once on startup.
     */
    private void initAptFile() {
        int version = getAptVersion();
        File file = new File(fgRoot + "/Airports/apt.dat.gz");
        InputStream fin = null;
        if (file.exists() == false) {
            file = new File(fgRoot + "/Airports/apt.dat");
            try {
                fin = new java.io.FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NavEJB.class.getName() + ".initAptFile()").log(Level.SEVERE, null, ex);
                return;
            }
        } else {
            try {
                fin = new GZIPInputStream(new java.io.FileInputStream(file));
            } catch (IOException ex) {
                Logger.getLogger(NavEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        try {
            AptParser parser = null;
            if (version == 810) {
                parser = new Apt810Parser();
            }
            if (version == 850 || version == 1000) {
                parser = new Apt850Parser();
            }
            aptRecords = parser.parse(fin);
            fin.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Returns the version number in the header of the apt.dat or apt.dat.gz
     * file
     *
     * @return
     */
    private int getAptVersion() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".getAptVersion");
        File file = new File(fgRoot + "/Airports/apt.dat.gz");
        InputStream fin = null;
        int retVersion = 810;
        if (file.exists() == false) {
            file = new File(fgRoot + "/Airports/apt.dat");
            try {
                fin = new java.io.FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NavEJB.class.getName() + ".getAptVersion()").log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                fin = new GZIPInputStream(new java.io.FileInputStream(file));
            } catch (IOException ex) {
                Logger.getLogger(NavEJB.class.getName() + ".getAptVersion()").log(Level.SEVERE, null, ex);
            }
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.
            reader.close();
            fin.close();
            log.info("Return Apt version: " + versionId);
            retVersion = versionId.intValue();
        } catch (IOException ioe) {
            //
        }
        return retVersion;
    }

    /**
     * Returns the version number in the header of the awy.dat or awy.dat.gz
     * file
     *
     * @return
     */
    private int getAwyVersion() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".getAwyVersion");
        File file = new File(fgRoot + "/Navaids/awy.dat.gz");
        InputStream fin = null;
        int retVersion = 810;
        if (file.exists() == false) {
            file = new File(fgRoot + "/Navaids/awy.dat");
            try {
                fin = new java.io.FileInputStream(file);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(NavEJB.class.getName() + ".getAptVersion()").log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                fin = new GZIPInputStream(new java.io.FileInputStream(file));
            } catch (IOException ex) {
                Logger.getLogger(NavEJB.class.getName() + ".getAptVersion()").log(Level.SEVERE, null, ex);
            }
        }
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String header1 = reader.readLine();
            String header2 = reader.readLine();
            Integer versionId = new Integer(header2.split(" ", 2)[0]);  //limit split to first token, then just get the 0th array element.
            reader.close();
            fin.close();
            log.info("Return Apt version: " + versionId);
            retVersion = versionId.intValue();
        } catch (IOException ioe) {
            //
        }
        return retVersion;
    }

    /**
     * Searches the apt.dat records for an airport with a full name that starts
     * with supplied string.
     *
     * @param begins
     * @return
     */
    public List<AptRecord> findMatchingAirports(String begins) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".findMatchingAirports");
        log.info("Search airports matching: " + begins);
        List retList = new ArrayList();

        for (Iterator m = this.aptRecords.values().iterator(); m.hasNext();) {
            AptRecord rec = (AptRecord) m.next();
            log.fine("Airport row type: " + rec.rowType);
            if (rec.rowType == Apt810Parser.ROW_LAND_AIRPORT_HEADER) {
                AptLandRecord aptRec = (AptLandRecord) rec;
                log.finest("Land Record name: " + aptRec.airportName + ", icao: " + aptRec.icaoId);
                if (aptRec.airportName.startsWith(begins)) {
                    retList.add(rec);
                }
            }
        }
        return retList;
    }

    /**
     * Searches apt.dat records for all airports that match the supplied ICAO
     * code.
     *
     * @param icao
     * @return
     */
    public List<AptRecord> findMatchingICAO(String icao) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".findMatchingICAO");
        log.fine("Search airports icao: " + icao);
        List retList = new ArrayList();
        log.fine("there are " + aptRecords.size() + " records to search");
        //log.info("Hash key contains: "+((AptLandRecord)this.aptRecords.get(icao)).airportName);

        Collection<AptRecord> aptList = this.aptRecords.values();
        for (Iterator m = aptList.iterator(); m.hasNext();) {
            AptRecord rec = (AptRecord) m.next();
            log.finer("Airport row type: " + rec.rowType);
            if (rec.rowType == Apt810Parser.ROW_LAND_AIRPORT_HEADER) {
                AptLandRecord aptRec = (AptLandRecord) rec;
                log.finest("aptRec icao: " + aptRec.icaoId + ", supplied: " + icao);
                if (aptRec.icaoId.startsWith(icao)) {
                    retList.add(aptRec);
                    log.fine("aptRecord: " + aptRec.icaoId + " is equal: " + icao);
                }
            }
        }
        return retList;
    }

    
    /**
     * 
     * @param lat
     * @param lon
     * @param km
     * @return 
     */
    public List<NavRecord> findNearbyNavaids(float lat, float lon, int km) {
        List retList = new ArrayList();

        LatLon[] box = Geo.calculateBoundingBoxByDistanceNm(lat, lon, 10);
        double maxLat = box[0].lat;
        double maxLon = box[0].lon;
        double minLat = box[1].lat;
        double minLon = box[1].lon;
        for (NavRecord nr : this.navRecords) {
            if (nr.latDecimal > minLat && nr.latDecimal < maxLat && nr.lonDecimal > minLon && nr.lonDecimal < maxLon) {
                retList.add(nr);
            }
        }
        return retList;
    }

    /**
     * 
     * @param lat
     * @param lon
     * @param km
     * @return 
     */
    public List<AirwayRecord> findNearbyAirways(float lat, float lon, int km) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".findNearbyAirways");
        log.info("Search nearby airways radius: "+km);
        List retList = new ArrayList();
        LatLon[] box = Geo.calculateBoundingBoxByDistanceNm(lat, lon, km*Geo.METRE2NM);
        double maxLat = box[0].lat;
        double maxLon = box[0].lon;
        double minLat = box[1].lat;
        double minLon = box[1].lon;
        for (Iterator ar = this.awyRecords.keySet().iterator(); ar.hasNext();) {
            String awId = (String) ar.next();
            List<AirwayRecord> recList = this.awyRecords.get(awId);
            for (Iterator r = recList.iterator(); r.hasNext();) {
                AirwayRecord rec = (AirwayRecord) r.next();
                log.fine("minLat: "+minLat+", maxLat: "+maxLat+", rec.beginLat: "+rec.beginLatDecimal+", rec.endLat:"+rec.endLatDecimal);
                log.fine("minLon: "+minLon+", maxLon: "+maxLon+", rec.beginLon: "+rec.beginLonDecimal+", rec.endLon:"+rec.endLonDecimal);
                if (rec.beginLatDecimal > minLat && rec.beginLatDecimal < maxLat && rec.beginLonDecimal > minLon && rec.beginLonDecimal < maxLon) {
                    retList.add(rec);
                    log.fine("Added airway: "+rec.airwayName);
                }
                if (rec.endLatDecimal > minLat && rec.endLatDecimal < maxLat && rec.endLonDecimal > minLon && rec.endLonDecimal < maxLon) {
                    retList.add(rec);
                    log.fine("Added airway: "+rec.airwayName);
                }
            }
        }

        return retList;
    }
}
