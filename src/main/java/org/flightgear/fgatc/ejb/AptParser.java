/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.ejb;

import java.io.InputStream;
import java.util.HashMap;
import org.flightgear.fgatc.ejb.dto.AptRecord;

/**
 *
 * @author scotth
 */
public abstract class AptParser {
    
   public abstract HashMap<String, AptRecord> parse(InputStream fin);

   protected String concat(String[] parts, int start, int end) {
        StringBuffer retStr = new StringBuffer();
        for(int p = start; p != end; p++) {
            if (p > start) {
                retStr.append(" ");
            }
            retStr.append(parts[p]);
        }
        return retStr.toString();
    }

}
