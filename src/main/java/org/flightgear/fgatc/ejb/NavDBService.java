/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.ejb;

import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import org.flightgear.fgatc.ejb.dto.AptRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;

/**
 *
 * @author scotth
 */
@Stateless
@LocalBean
//@Path("navdb")
public class NavDBService {
    @EJB
    NavEJB navEJB;

    /**
     * return list of matching airports
     * 
     * @param icaoId
     * @return 
     */
    //@GET
    //@Path("airport/{airport}")
    //@Produces({"application/xml", "application/json"})
    //public List<AptRecord> findAirport(@PathParam("airport") String icaoId) {
    //    Logger log = Logger.getLogger(this.getClass().getName() + ".findAirport");
    //    return (List<AptRecord>)navEJB.findMatchingICAO(icaoId);
    //}

    
}
