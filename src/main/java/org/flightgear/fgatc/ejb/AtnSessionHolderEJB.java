/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.ejb;

import org.flightgear.fgatc.AtnRequest;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Singleton;
import javax.ejb.LocalBean;
import org.flightgear.fgatc.AtnSession;

/**
 *
 * @author scotth
 */
@Singleton
@LocalBean
public class AtnSessionHolderEJB {

    private Map sessionList = new HashMap<String, AtnSession>();
    private Map requestList = Collections.synchronizedMap(new HashMap<String, AtnRequest>());
    private int nextRequestSeq = 1;

    public AtnSession findSession(String id) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".findSession");
        log.info("Search for session: " + id);
        for (Iterator s = this.sessionList.keySet().iterator(); s.hasNext();) {
            log.log(Level.INFO, "session key: {0}", s.next());
        }
        return (AtnSession) this.sessionList.get(id);
    }

    public void addSession(AtnSession sess) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".addSession");
        log.info("Update for session: " + sess.sessionId);
        this.sessionList.put(sess.sessionId, sess);
    }

    public List<AtnSession> getAllSessions() {
        List retList = new ArrayList();
        for (Iterator s = this.sessionList.keySet().iterator(); s.hasNext();) {
            String key = (String) s.next();
            AtnSession val = (AtnSession) this.sessionList.get(key);
            retList.add(val);
        }
        return retList;
    }

    public void removeSession(String id) {
        List<AtnRequest> requestList = this.getAllRequests();
        for (Iterator r = requestList.iterator(); r.hasNext();) {
            AtnRequest req = (AtnRequest) r.next();
            if (req.session.sessionId == id) {
                this.requestList.remove(req.id);
            }
        }
        this.sessionList.remove(id);

    }

    public AtnRequest findRequest(String id) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".findRequest");
        log.info("Search for request: " + id);
        //for (Iterator s = this.requestList.keySet().iterator(); s.hasNext();) {
        //    log.log(Level.INFO, "session key: {0}", s.next());
        //}
        return (AtnRequest) this.requestList.get(id);
    }

    public void addRequest(AtnRequest req) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".addRequest");
        if (req.id == null) {
            log.info("New request of type: " + req.requestType);
            req.id = String.valueOf(this.nextRequestSeq++);
        } else {
            log.info("Update request for id: " + req.id);
        }
        synchronized (requestList) {
            this.requestList.put(req.id, req);
        }
        log.info("Created new request: " + req.id + " of type: " + req.requestType);
    }

    public List<AtnRequest> getAllRequests() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".getAllRequest");
        List retList = new ArrayList();
        synchronized (requestList) {
            for (Iterator s = this.requestList.keySet().iterator(); s.hasNext();) {
                String key = (String) s.next();
                log.fine("Get Request for key: " + key);
                AtnRequest val = (AtnRequest) this.requestList.get(key);
                if (val != null) {
                    retList.add(val);
                } else {
                    log.warning("AtnRequest was null for key: " + key);
                }
            }
        }
        return retList;
    }
    
    

    public void removeRequest(String id) {
        synchronized(requestList) {
            this.requestList.remove(id);
        }
    }
    
    public void setRequestStatus(String id, String status) {
        synchronized(requestList) {
            AtnRequest req = (AtnRequest) requestList.get(id);
            req.setStatus(status);
            requestList.put(req.id, req);
        }
    }
}
