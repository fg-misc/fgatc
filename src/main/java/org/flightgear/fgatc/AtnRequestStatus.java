/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc;

/**
 *
 * @author scotth
 */
public enum AtnRequestStatus {
    REQUESTED, RECEIVED, ACCEPTED, REJECTED;
}
