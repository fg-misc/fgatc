/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.dao;

/**
 *
 * @author scotth
 */
public abstract class DAOFactory {

    public abstract MaintRecordDAO getATNMaintRecordDAO();

    public static DAOFactory getDAOFactory() {
        return new CassandraDAO();
    }
}
