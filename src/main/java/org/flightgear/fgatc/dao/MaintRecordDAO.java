/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.dao;

import java.util.Collection;

/**
 *
 * @author scotth
 */
public interface MaintRecordDAO {
  public int insertMaintRecord(ATNMaintRecord item);
  public boolean deleteATNMaintRecord(ATNMaintRecord item);
  public ATNMaintRecord findMaintRecord(String key);
  public boolean updateCustomer(ATNMaintRecord item);
  public Collection selectCustomersTO();
}
