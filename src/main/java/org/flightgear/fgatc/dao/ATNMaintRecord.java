/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
//import javax.persistence.Column;
import javax.persistence.Entity;
//import javax.persistence.Id;
import javax.persistence.Table;
import me.prettyprint.hom.annotations.Column;
import me.prettyprint.hom.annotations.Id;
import org.apache.cassandra.utils.avro.UUID;

/**
 *
 * @author scotth
 */
@Entity
@Table(name = "FGDev")
public class ATNMaintRecord implements Serializable {

    @Id
    private UUID id;
    
    @Column(name = "sessionid")
    private String sessionId;
    
    @Column(name = "eventtype")
    private String eventType;
    
    private Map<String, String> anonymousProps = new HashMap<String, String>();

    public void addAnonymousProp(String name, String value) {
        anonymousProps.put(name, value);
    }

    public Collection<Entry<String, String>> getAnonymousProps() {
        return anonymousProps.entrySet();
    }
    
      public String getAnonymousProp(String name) {
    return anonymousProps.get(name);
  }

    /**
     * @return the id
     */
    public UUID getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(UUID id) {
        this.id = id;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
    
    
}
