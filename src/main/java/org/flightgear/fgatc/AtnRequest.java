/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.log4j.Logger;

/**
 *
 * @author scotth
 */
public class AtnRequest implements Serializable {
    public String id;
    public String requestType;          //DEPART CLEARANCE, GATE REQUEST, ALT MOD, 
    public Map<String, String> requestParams = new HashMap<String, String>();   //  parameter values for request
    public AtnSession session;          // foreign key to AtnSession;
    private String  status;    // REQUESTED, RECEIVED, APPROVED, REJECTED
    public boolean active = true;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the requestType
     */
    public String getRequestType() {
        return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    /**
     * @return the requestParams
     */
    public Map<String, String> getRequestParams() {
        return requestParams;
    }

    /**
     * @param requestParams the requestParams to set
     */
    public void setRequestParams(Map<String, String> requestParams) {
        this.requestParams = requestParams;
    }

    /**
     * @return the session
     */
    public AtnSession getSession() {
        return session;
    }

    /**
     * @param session the session to set
     */
    public void setSession(AtnSession session) {
        this.session = session;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        Logger log = Logger.getLogger(this.getClass().getName()+".setStatus");
        log.info("AtnRequest id: "+this.id+", old status: "+this.status+", new status: "+status);
        this.status = status;
    }
    
    public String getRequestFormatted() {
        StringBuilder retStr = new StringBuilder();
        for(Iterator p = this.requestParams.keySet().iterator(); p.hasNext();) {
            String key = (String) p.next();
            String value = this.requestParams.get(key);
            retStr.append(key+": "+value+"<br/>");
        }
        return retStr.toString();
    }
    
}