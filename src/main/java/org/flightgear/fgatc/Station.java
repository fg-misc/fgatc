/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc;

import java.io.Serializable;
import java.util.logging.Logger;

/**
 *
 * @author scotth
 */
public class Station implements Serializable {
    private String  username;
    private String  airportIcaoId;
    private boolean active;
    private String  role;  // ground clearance, tower, approach, departures etc
    private float   fgcomFrequency;  // active frequency
    private String  callsign;   // from mpmap

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the airportIcaoId
     */
    public String getAirportIcaoId() {
        return airportIcaoId;
    }

    /**
     * @param airportIcaoId the airportIcaoId to set
     */
    public void setAirportIcaoId(String airportIcaoId) {
        Logger log = Logger.getLogger(this.getClass().getName()+".setAirportIcaoId");
        log.info("set icao: "+airportIcaoId);
        this.airportIcaoId = airportIcaoId;
    }

    /**
     * @return the active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * @param active the active to set
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the fgcomFrequency
     */
    public float getFgcomFrequency() {
        return fgcomFrequency;
    }

    /**
     * @param fgcomFrequency the fgcomFrequency to set
     */
    public void setFgcomFrequency(float fgcomFrequency) {
        this.fgcomFrequency = fgcomFrequency;
    }

    /**
     * @return the callsign
     */
    public String getCallsign() {
        return callsign;
    }

    /**
     * @param callsign the callsign to set
     */
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

}
