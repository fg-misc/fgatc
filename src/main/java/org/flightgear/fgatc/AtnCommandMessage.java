/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc;

import java.io.Serializable;

/**
 *
 * @author scotth
 */
public class AtnCommandMessage implements Serializable {
    public String command;
    public String commandMessage;
    public String msgLine2;
    public String commandAirport;

    /**
     * @return the command
     */
    public String getCommand() {
        return command;
    }

    /**
     * @param command the command to set
     */
    public void setCommand(String command) {
        this.command = command;
    }

    /**
     * @return the commandMessage
     */
    public String getCommandMessage() {
        return commandMessage;
    }

    /**
     * @param commandMessage the commandMessage to set
     */
    public void setCommandMessage(String commandMessage) {
        this.commandMessage = commandMessage;
    }

    /**
     * @return the msgLine2
     */
    public String getMsgLine2() {
        return msgLine2;
    }

    /**
     * @param msgLine2 the msgLine2 to set
     */
    public void setMsgLine2(String msgLine2) {
        this.msgLine2 = msgLine2;
    }

    /**
     * @return the commandAirport
     */
    public String getCommandAirport() {
        return commandAirport;
    }

    /**
     * @param commandAirport the commandAirport to set
     */
    public void setCommandAirport(String commandAirport) {
        this.commandAirport = commandAirport;
    }
}
