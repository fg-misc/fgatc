/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.web;

import java.util.logging.Logger;

/**
 *
 * @author scotth
 */
public class AirportInfo {
    public float lat;
    public float lon;
    public String icaoId;
    public String fullName;

    /**
     * @return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        Logger log = Logger.getLogger(this.getClass().getName()+"setLatFloat");
        log.fine("Set lat: "+lat);
        this.lat = lat;
    }

    public void setLat(String lat) {
        Logger log = Logger.getLogger(this.getClass().getName()+"setLatString");
        log.fine("Set lat: "+lat);
        this.lat = Float.valueOf(lat).floatValue();
    }

    /**
     * @return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return the icaoId
     */
    public String getIcaoId() {
        return icaoId;
    }

    /**
     * @param icaoId the icaoId to set
     */
    public void setIcaoId(String icaoId) {
        this.icaoId = icaoId;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
