/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.flightgear.fgatc.web;

import java.util.logging.Level;
import javax.ejb.EJB;
import org.flightgear.fgatc.NoStationFoundException;
import org.flightgear.fgatc.Station;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;
import org.flightgear.fgatc.*;
import org.flightgear.fgatc.ejb.AtcEJB;
import org.flightgear.fgatc.ejb.AtcSessionFacade;
import org.flightgear.fgatc.ejb.AtnSessionHolderEJB;
import org.flightgear.fgatc.ejb.NavEJB;
import org.flightgear.fgatc.ejb.dto.AirwayRecord;
import org.flightgear.fgatc.ejb.dto.AptRecord;
import org.flightgear.fgatc.ejb.dto.NavRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptCommsRecord;
import org.flightgear.fgatc.ejb.dto.apt.AptLandRecord;
import org.flightgear.fgatc.ejb.entity.FlightPlan;
import org.ol4jsf.util.WKTFeaturesCollection;


/**
 *
 * @author scotth
 */
@ManagedBean(name = "atc")
@SessionScoped
public class AtcBean implements Serializable {

    @EJB
    private NavEJB navEJB;
    @EJB
    private AtcEJB atcEJB;
    @EJB
    private AtcSessionFacade facadeEJB;
    @EJB
    private AtnSessionHolderEJB atnGlobal;
    
    @ManagedProperty("#{user}")
    private UserBean usr;
    private AirportInfo currentAirport = new AirportInfo();
    private int  zoomLevel = 10;
    private Station activeStation = new Station();
    private String  currentCallsign = "";
    private AircraftCharacteristics currentCharacteristics = new AircraftCharacteristics();
    private AtnSession currentATN;
    private AtnRequest        currentRequest;

    /** Creates a new instance of AtcBean */
    public AtcBean() {
        this.currentAirport = new AirportInfo();
        this.currentAirport.lat = (float) -33.940797;
        this.currentAirport.lon = (float) 151.174450;
        this.currentAirport.icaoId = "YSSY";
        this.activeStation.setActive(false);

    }

    public String updateLocation() {
        Logger log = Logger.getLogger(this.getClass().getName() + "updateLocation");
        log.info("Ajax update of location info");
        log.info("New value of lat: " + this.currentAirport.lat);
        return "ok";
    }

    public String clearToLand() {
        Logger log = Logger.getLogger(this.getClass().getName() + "clearToLand");
        log.info("Callsign "+this.currentCallsign+" is cleared to LAND");
        return "ok";
    }

    /**
     * @return the currentAirport
     */
    public AirportInfo getCurrentAirport() {
        Logger log = Logger.getLogger(this.getClass().getName() + "getCurrentAirport");
        log.finer("Return current airport");
        return currentAirport;
    }

    /**
     * @param currentAirport the currentAirport to set
     */
    public void setCurrentAirport(AirportInfo currentAirport) {
        this.currentAirport = currentAirport;
    }
    
    
    public String preATC() {
        Logger log = Logger.getLogger(this.getClass().getName() + "preATC");
        log.info("Set up object for ATC.");
        String retStr = "/atc/index";
        if (this.isNotActive() == true) {
            retStr = "/atc/index";
        } else {
            retStr = "/atc/basicController";
        }
        return retStr;
    }

    /**
     * @return the zoomLevel
     */
    public int getZoomLevel() {
        return zoomLevel;
    }

    /**
     * @param zoomLevel the zoomLevel to set
     */
    public void setZoomLevel(int zoomLevel) {
        Logger log = Logger.getLogger(this.getClass().getName()+".setZoomLevel");
        log.info("update zoom to new level: "+zoomLevel);
        this.zoomLevel = zoomLevel;
    }

    /**
     * @return the usr
     */
    public UserBean getUsr() {
        return usr;
    }

    /**
     * @param usr the usr to set
     */
    public void setUsr(UserBean usr) {
        this.usr = usr;
    }

    /**
     * @return the activeStation
     */
    public Station getActiveStation() {
        return activeStation;
    }

    /**
     * @param activeStation the activeStation to set
     */
    public void setActiveStation(Station activeStation) {
        this.activeStation = activeStation;
    }

    /**
     * @return the currentCharacteristics
     */
    public AircraftCharacteristics getCurrentCharacteristics() {
        return currentCharacteristics;
    }

    /**
     * @param currentCharacteristics the currentCharacteristics to set
     */
    public void setCurrentCharacteristics(AircraftCharacteristics currentCharacteristics) {
        this.currentCharacteristics = currentCharacteristics;
    }

    public List<AirportInfo> autoQueryAirport(String query) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".autoQueryAirport");
        List retList = new ArrayList();
        log.info("Autocomplete query: " + query);
        List aptList = navEJB.findMatchingAirports(query);
        log.info("match AirportName returned: " + aptList.size());
        for (Iterator a = aptList.iterator(); a.hasNext();) {
            AptLandRecord apt = (AptLandRecord) a.next();
            AirportInfo item = new AirportInfo();
            item.fullName = apt.airportName;
            item.icaoId = apt.icaoId;
            log.fine("Query found airport icao: " + item.icaoId);
            if (apt.runways.size() > 0) {
                item.lat = apt.runways.get(0).runwayLat;
                item.lon = apt.runways.get(0).runwayLon;
                retList.add(item);
            }
        }
        if (retList.size() == 0) {
            aptList = navEJB.findMatchingICAO(query.toUpperCase());
            log.fine("match ICAO returned: " + aptList.size());
            for (Iterator a = aptList.iterator(); a.hasNext();) {
                AptLandRecord apt = (AptLandRecord) a.next();
                AirportInfo item = new AirportInfo();
                item.fullName = apt.airportName;
                item.icaoId = apt.icaoId;
                if (apt.runways.size() > 0) {
                    item.lat = apt.runways.get(0).runwayLat;
                    item.lon = apt.runways.get(0).runwayLon;
                    retList.add(item);
                }
            }
        }
        log.info("Returned: " + retList.size());
        return retList;
    }

    
    public List<String> autoQueryICAOSimple(String query) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".autoQueryICAO");
        log.info("Query: "+query);
        List retList = new ArrayList();
        if (query != null && query.length() > 0) {
            List aptList = navEJB.findMatchingICAO(query.toUpperCase());
            for (Iterator a = aptList.iterator(); a.hasNext();) {
                AptLandRecord apt = (AptLandRecord) a.next();
                log.fine("query icao: " + apt.icaoId + ", fullName: " + apt.airportName + " has " + apt.runways.size() + " runways");
                retList.add(apt.icaoId);
            }
        }
        log.info("Returned: " + retList.size());
        return retList;
    }
    /**
     * 
     * @param query
     * @return 
     */
    public List<AirportInfo> autoQueryICAO(String query) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".autoQueryICAO");
        log.info("Query: "+query);
        List retList = new ArrayList();
        if (query != null && query.length() > 0) {
            List aptList = navEJB.findMatchingICAO(query.toUpperCase());
            for (Iterator a = aptList.iterator(); a.hasNext();) {
                AptLandRecord apt = (AptLandRecord) a.next();
                log.fine("query icao: " + apt.icaoId + ", fullName: " + apt.airportName + " has " + apt.runways.size() + " runways");
                AirportInfo item = new AirportInfo();
                item.fullName = apt.airportName;
                item.icaoId = apt.icaoId;
                if (apt.runways.size() > 0) {
                    item.lat = apt.runways.get(0).runwayLat;
                    item.lon = apt.runways.get(0).runwayLon;
                }
                retList.add(item);
            }
        } else {
            retList.add(this.currentAirport);
        }
        log.info("Returned: " + retList.size());
        return retList;
    }

    /**************************
    public List<String> getAutoQueryICAO() {
    String query = Faces.var("searchString", String.class);
    ArrayList retList = new ArrayList();
    Logger log = Logger.getLogger(this.getClass().getName()+".getQueryICAO");
    log.info("searchString: "+query);
    if (query != null && query.length() > 0) {
    List aptList = navEJB.findMatchingICAO(query);
    for (Iterator a = aptList.iterator(); a.hasNext();) {
    AptLandRecord apt = (AptLandRecord) a.next();
    retList.add(apt.icaoId);
    }
    }
    log.info("Returned: "+retList.size());
    return retList;
    }
     ****************************/
    public String createStation() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".createStation");
        this.activeStation.setUsername(usr.username);
        this.activeStation.setAirportIcaoId(this.currentAirport.icaoId);
        List<AptRecord> aptList = navEJB.findMatchingICAO(this.activeStation.getAirportIcaoId());
        log.info("There are " + aptList.size() + " airports matching ICAO: " + this.activeStation.getAirportIcaoId());
        AptLandRecord apt = (AptLandRecord) aptList.get(0);
        if (apt.hasATC == true) {
            AirportInfo info = new AirportInfo();
            info.fullName = apt.airportName;
            info.icaoId = apt.icaoId;
            info.lat = apt.tower.towerLat;
            info.lon = apt.tower.towerLon;
            this.currentAirport = info;
            if (activeStation.getFgcomFrequency() == 0) {
                for (Iterator c = apt.radio.iterator(); c.hasNext();) {
                    AptCommsRecord comm = (AptCommsRecord)c.next();
                    if (activeStation.getRole().equalsIgnoreCase("TWR") && comm.rowType == 54) {
                        activeStation.setFgcomFrequency((float) comm.frequencyMHz);
                    }
                    if (activeStation.getRole().equalsIgnoreCase("GND") && comm.rowType == 53) {
                        activeStation.setFgcomFrequency((float) comm.frequencyMHz);
                    }
                    if (activeStation.getRole().equalsIgnoreCase("APP") && comm.rowType == 55) {
                        activeStation.setFgcomFrequency((float) comm.frequencyMHz);
                    }
                    if (activeStation.getRole().equalsIgnoreCase("DEP") && comm.rowType == 55) {
                        activeStation.setFgcomFrequency((float) comm.frequencyMHz);
                    }
                }
            }
            atcEJB.addNewStation(activeStation);
        }
        return "basicController";
    }

    public List<Station> getLoginStations() {
        List retList = atcEJB.findAllActiveStations();
        return retList;
    }

    public List<FlightPlan> getActivePlans() {
        return facadeEJB.getActiveFlightPlans(this.activeStation.getAirportIcaoId(), 10);
    }

    public boolean isNotActive() {
        boolean retStatus = true;
        if (this.activeStation != null && this.activeStation.isActive() == true) {
            retStatus = false;
        }
        return retStatus;
    }

    public String readyNewChar() {
        return "newAircraftCharacteristics";
    }

    public String signoutStation() {
        try {
            Station myStation = atcEJB.getStationByUser(usr.getUsername());
            this.activeStation.setActive(false);
            this.activeStation.setAirportIcaoId(null);
            atcEJB.removeStation(myStation);
        } catch (NoStationFoundException ex) {
            Logger.getLogger(AtcBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "index";
    }

    public List<MultiPlayer> getMultiplayers() {
        return atcEJB.getAllMultiplayers();
    }

    public List<MultiPlayer> getMpATC() {
        List retList = new ArrayList();
        List mpList = atcEJB.getAllMultiplayers();
        for (Iterator m = mpList.iterator(); m.hasNext();) {
            MultiPlayer player = (MultiPlayer) m.next();
            if (player.aircraftType.toLowerCase().matches(".*atc.*") == true) {
                retList.add(player);
            }
        }
        return retList;
    }

    public List<MultiPlayer> getAreaAircraftMarkers() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getAreaAircraftMarker");
        List retList = new ArrayList();
        List<MultiPlayer> mpList = atcEJB.getAllMultiplayers();
        log.info("Find mp within lat: "+this.currentAirport.lat+", lon: "+this.currentAirport.lon);
        LatLon[] box = Geo.calculateBoundingBoxByDistanceNm(this.currentAirport.lat, this.currentAirport.lon, 40);
        double maxLat = box[0].lat;
        double maxLon = box[0].lon;
        double minLat = box[1].lat;
        double minLon = box[1].lon;
        log.fine("box - lat1: "+maxLat+", lon1: "+maxLon+"   lat2: "+minLat+", lon2: "+minLon);
        for(MultiPlayer mp : mpList) {
            log.finer("    mp.lat: "+mp.lat+"> "+minLat+" < "+maxLat+" and  mp.lon: "+mp.lon+" >"+minLon+" <"+maxLon);
            if (mp.lat > minLat && mp.lat < maxLat && mp.lon > minLon && mp.lon < maxLon) {
                retList.add(mp);
                log.fine("Added MP: "+mp.callsign);
            }
        }

        //Where Lat>$minLat And Lat<$maxLat
        //And Lon>$minLon And Lon<$maxLon
        return retList;
    }
    
    public List<AirwayRecord> getAreaAirways() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getAreaAirways");
        List<AirwayRecord> retList = navEJB.findNearbyAirways(this.currentAirport.lat, this.currentAirport.lon, this.getZoomLevel()*20);
        log.info("Returned "+retList.size()+" airway records");
        return retList;
    }

    public String getInfoBoxHTML() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        MultiPlayer mark = ctx.getApplication().evaluateExpressionGet(ctx, "#{mark}", MultiPlayer.class);
        String retStr = "<div class=\\'infobox\\'>"+mark.callsign+"<br/>"+mark.aircraftType+"<br/>"+mark.elevation+"</div>";
        return retStr;
    }

    public String getIconCurrentStatusHTML() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getIconCurrentStatusHTML");
        StringBuffer retStr = new StringBuffer();
        FacesContext ctx = FacesContext.getCurrentInstance();
        MultiPlayer mark = ctx.getApplication().evaluateExpressionGet(ctx, "#{mark}", MultiPlayer.class);
        //log.info("get ICON for mark: "+mark.callsign);
        retStr.append("<br/><input type=submit value=\\\"clear to land\\\" onClick=\\\"return ajaxLand(\\\""+mark.callsign+"\\\");\\\"/>");
        return retStr.toString();
    }

    public String getFixesVectors() {
        Logger log = Logger.getLogger(this.getClass().getName()+".getFixesVectors");
        log.info("Get nearby fixes...");
        List<String> wkts = new ArrayList();
        List<NavRecord> navList = navEJB.findNearbyNavaids(this.currentAirport.lat, this.currentAirport.lon, 10);
        for(NavRecord nr : navList) {
            String point = "POINT("+nr.lonDecimal+" "+nr.latDecimal+")";
            wkts.add(point);
        }

        WKTFeaturesCollection features = new WKTFeaturesCollection();
        features.addAllFeatures(wkts);
        String featuresStr = features.toMap();
        log.info("Returned features >>"+featuresStr+"<<");
        return featuresStr;

        //return "Features: POINT(151.1801147461 -33.832397460938)%POINT(151.20895385743 -34.068603515626)%POINT(151.29684448243 -34.016418457032)%POINT(150.93704223633 -33.993072509766)";
    }

    public String getFirVectors() {
        List<String> wkts = new ArrayList();

        WKTFeaturesCollection features = new WKTFeaturesCollection();
        features.addAllFeatures(wkts);
        return features.toMap();
    }

    public int getIconOffsetX() {
        int[] offset = {0,-4,-4,-4,-3,-3,-3,-2,-2,-20,-20,-15,-15,-5,-5,0,3,8,9,10,10};
        return offset[this.zoomLevel];
    }

    public int getIconOffsetY() {
        int[] offset = {0,-4,-4,-4,-3,-3,-3,-2,-2,-20,-20,-15,-15,-5,-5,0,3,8,9,10,10};
        return offset[this.zoomLevel];
    }

    /**
     * @return the currentCallsign
     */
    public String getCurrentCallsign() {
        return currentCallsign;
    }

    /**
     * @param currentCallsign the currentCallsign to set
     */
    public void setCurrentCallsign(String currentCallsign) {
        this.currentCallsign = currentCallsign;
    }
    
    
    public List<AtnSession> getAllATNSessions() {
        return this.atnGlobal.getAllSessions();
    }

    /**
     * @return the currentATN
     */
    public AtnSession getCurrentATN() {
        return currentATN;
    }
    
        /**
     * @return the currentRequest
     */
    public AtnRequest getCurrentRequest() {
        return currentRequest;
    }

    /**
     * @param currentRequest the currentRequest to set
     */
    public void setCurrentRequest(AtnRequest currentRequest) {
        this.currentRequest = currentRequest;
    }

    /**
     * @param currentATN the currentATN to set
     */
    public void setCurrentATN(AtnSession currentATN) {
        this.currentATN = currentATN;
    }

        public List<AtnRequest> getCurrentRequestList() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doCurrentRequestList");
        List retList = new ArrayList();
        List currList = atnGlobal.getAllRequests();
        for(Iterator r = currList.iterator(); r.hasNext();) {
            AtnRequest req = (AtnRequest)r.next();
            log.fine("Request id: "+req.id+" of type: "+req.requestType+", status: "+req.getStatus());
            if (req != null && req.getStatus() != null && req.getStatus().equalsIgnoreCase("ACCEPTED") != true && req.getStatus().equalsIgnoreCase("REJECTED") != true) {
                retList.add(req);
            }
        }
        return retList;
    }
    
    public void atnRequestUpdate(AjaxBehaviorEvent event) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".atnRequestUpdate");
        log.info("ajax update event for current request: "+currentRequest.id+", new status: "+currentRequest.getStatus());
        atnGlobal.setRequestStatus(currentRequest.id, currentRequest.getStatus());
        String xid = this.currentRequest.session.sessionId;
        AtnSession sess = atnGlobal.findSession(xid);
        AtnCommandMessage msg = new AtnCommandMessage();
        msg.command = "MAILBOX";
        msg.commandMessage = "DEPART CLEARANCE "+this.currentRequest.getStatus();
        msg.commandAirport = this.currentAirport.icaoId;
        sess.getCmdQueue().add(msg);
        atnGlobal.addSession(sess);
    }

}
