/*
 *  Copyright (C) 2010 scotth
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.web;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationCase;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.codec.binary.Base64;
import org.flightgear.fgatc.ejb.AtcSessionFacade;
import org.flightgear.fgatc.ejb.entity.User;

/**
 *
 * @author S.Hamilton
 */
@ManagedBean(name = "user")
@SessionScoped
public class UserBean extends SecurityAuthenticationBase implements Serializable {
    @EJB
    private AtcSessionFacade atcSessionFacade;

    private User currentUser;
    private String seedText;
    private String nameError;
    private String nameConfirm;


    /** helper resources **/
    //@PersistenceContext(name = "persistence/LogicalName", unitName = "FGATC-ejbPU")
    //private EntityManager em;
    
    //@Resource
    //private javax.transaction.UserTransaction utx;

    /** Creates a new instance of UserBean */
    public UserBean() {
    }

    /**
     * @return the currentUser
     */
    public User getCurrentUser() {
        return currentUser;
    }

    /**
     * @param currentUser the currentUser to set
     */
    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    /**
     * @return the seedText
     */
    public String getSeedText() {
        return seedText;
    }

    /**
     * @param seedText the seedText to set
     */
    public void setSeedText(String seedText) {
        this.seedText = seedText;
    }

    /**
     * @return the nameError
     */
    public String getNameError() {
        return nameError;
    }

    /**
     * @param nameError the nameError to set
     */
    public void setNameError(String nameError) {
        this.nameError = nameError;
    }

    /**
     * @return the nameConfirm
     */
    public String getNameConfirm() {
        return nameConfirm;
    }

    /**
     * @param nameConfirm the nameConfirm to set
     */
    public void setNameConfirm(String nameConfirm) {
        this.nameConfirm = nameConfirm;
    }


    /**
    protected void persist(Object object) {
        try {
            utx.begin();
            em.joinTransaction();
            em.persist(object);
            utx.commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }

    protected void update(Object object) {
        try {
            utx.begin();
            em.joinTransaction();
            em.merge(object);
            em.flush();
            utx.commit();
        } catch (Exception e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e);
            throw new RuntimeException(e);
        }
    }
     ***/

    /**
     * action to clear everything ready for new user input
     * @return
     */
    public String newUser() {
        this.currentUser = new User();
        this.nameError = "";
        this.nameConfirm = "";
        return "newUser";
    }

    public String saveUser() {

        String text = this.currentUser.getUsername() + "." + this.seedText;
        try {
            MessageDigest sha = MessageDigest.getInstance("MD2");
            byte[] cipher = sha.digest(text.getBytes());
            this.currentUser.setKeyValue(getEncoded(cipher));
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        atcSessionFacade.persist(this.currentUser);
        sendEmail();
        return "success";
    }

    private byte[] getDecoded(String string) {
        //Base64 b64 = new Base64();
        return Base64.decodeBase64(string.getBytes());
    }

    private String getEncoded(byte[] buffer) {
        //Base64 b64 = new Base64();
        String buff = new String(Base64.encodeBase64(buffer));
        return buff;
    }

    private void sendEmail() {
        try {
            Properties props = System.getProperties();
            String usr = this.currentUser.getUsername();
            String key = this.currentUser.getKeyValue();

            // Setup mail server
            props.put("mail.smtp.host", "localhost");

            // Get session
            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress("no-reply@localhost"));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(usr + "@localhost"));
            message.setSubject("Welcome");

            StringBuffer msg = new StringBuffer("\n\nWelcome");
            
            message.setText(msg.toString());

            // Send message
            Transport.send(message);

        } catch (AddressException ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchProviderException ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void validateUsername(ValueChangeEvent e) {
            nameError = "";
            nameConfirm = "";
            UIInput nameInput = (UIInput) e.getComponent();
            String name = (String) nameInput.getValue();
    }

    @Override
    public String authenticateUser() {
        String retPage = "login";
        Logger log = Logger.getLogger(getClass().getName());
        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletRequest req = (HttpServletRequest)ctx.getExternalContext().getRequest();

        User usr = atcSessionFacade.findUser(this.username);

        if (usr != null) {
            log.finest("Found user: "+this.username+" stored password: "+usr.getPassword());
            if (this.password.equalsIgnoreCase(usr.getPassword())) {
                this.authenticated = true;
                retPage = "index";
            } else {
                log.warning("provided password: "+this.password+" not the same as: "+usr.getPassword());
                this.authenticated = false;
                FacesMessage msg = new FacesMessage("Invalid Username or Password");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                ctx.addMessage(null, msg);
            }
        } else {
            this.authenticated = false;
            FacesMessage msg = new FacesMessage("Invalid Username or Password");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            ctx.addMessage(null, msg);
        }
        return retPage;
    }

    public void checkAuthenticated() {
        Logger log = Logger.getLogger(getClass().getName());
        log.fine("Check if we are authenticated: " + this.isAuthenticated());
        if (this.isAuthenticated() == false) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ExternalContext extCtx = ctx.getExternalContext();
            String rootId = ctx.getViewRoot().getViewId();
            log.log(Level.INFO, "this rootId: " + rootId);
            ConfigurableNavigationHandler nav =
                    (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();

            NavigationCase loginCase = nav.getNavigationCase(ctx, null, "login");
            NavigationCase expCase   = nav.getNavigationCase(ctx, null, "expired");
            String loginId = ".";
            if (loginCase != null) {
                loginId = loginCase.getToViewId(ctx);
            }
            String expireId = ".";
            if (expCase != null) {
                expireId= expCase.getToViewId(ctx);
            }
            String protectPages = extCtx.getInitParameter("fgatc.security.PROTECT_REGEX");
            if (protectPages == null || protectPages.length() == 0) {
                protectPages = ".*";
            }
            String publicPages = extCtx.getInitParameter("fgatc.security.PUBLIC_REGEX");
            if (publicPages == null || publicPages.length() == 0) {
                publicPages = "";
            }
            String loginSubStr = loginId.substring(0, loginId.lastIndexOf("."));
            String expireSubStr = expireId.substring(0,expireId.indexOf("."));
            log.log(Level.INFO, "Check if current page: " + rootId + ", does not contain '" + loginSubStr + "' or matches: " + protectPages + ", but doesn't match public: " + publicPages);
            if (rootId.contains(loginSubStr) == false && rootId.contains(expireSubStr) == false && rootId.matches(protectPages) == true && rootId.matches(publicPages) == false) {
                //ctx.getApplication().getNavigationHandler().handleNavigation(ctx, null, "login");
                nav.performNavigation("login");
            }
        }
    }

    public String logout() {
        try {
            this.setAuthenticated(false);
            this.setUsername(null);
            this.setPassword(null);
            this.currentUser = null;
            FacesContext ctx = FacesContext.getCurrentInstance();
            HttpServletRequest req = (HttpServletRequest) ctx.getExternalContext().getRequest();
            req.logout();
            ((HttpSession) ctx.getExternalContext().getSession(false)).invalidate();
            //ctx.getApplication().getNavigationHandler().handleNavigation(ctx, null, "logout");
            return "logout";
        } catch (ServletException ex) {
            Logger.getLogger(UserBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "logout";
    }
}
