/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.web;

import java.util.List;

/**
 *
 * @author scotth
 */
public class AircraftCharacteristics {
    public String aircraftType;
    public int defaultFlightLevel;
    public double defaultCruiseSpeed;
    public int avgClimbFPM;
    public int avgDescentFPM;
    public String wakeCategory;
    public int  mtow;
    public List landingCategory;

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the defaultFlightLevel
     */
    public int getDefaultFlightLevel() {
        return defaultFlightLevel;
    }

    /**
     * @param defaultFlightLevel the defaultFlightLevel to set
     */
    public void setDefaultFlightLevel(int defaultFlightLevel) {
        this.defaultFlightLevel = defaultFlightLevel;
    }

    /**
     * @return the defaultCruiseSpeed
     */
    public double getDefaultCruiseSpeed() {
        return defaultCruiseSpeed;
    }

    /**
     * @param defaultCruiseSpeed the defaultCruiseSpeed to set
     */
    public void setDefaultCruiseSpeed(double defaultCruiseSpeed) {
        this.defaultCruiseSpeed = defaultCruiseSpeed;
    }

    /**
     * @return the avgClimbFPM
     */
    public int getAvgClimbFPM() {
        return avgClimbFPM;
    }

    /**
     * @param avgClimbFPM the avgClimbFPM to set
     */
    public void setAvgClimbFPM(int avgClimbFPM) {
        this.avgClimbFPM = avgClimbFPM;
    }

    /**
     * @return the avgDescentFPM
     */
    public int getAvgDescentFPM() {
        return avgDescentFPM;
    }

    /**
     * @param avgDescentFPM the avgDescentFPM to set
     */
    public void setAvgDescentFPM(int avgDescentFPM) {
        this.avgDescentFPM = avgDescentFPM;
    }

    /**
     * @return the wakeCategory
     */
    public String getWakeCategory() {
        return wakeCategory;
    }

    /**
     * @param wakeCategory the wakeCategory to set
     */
    public void setWakeCategory(String wakeCategory) {
        this.wakeCategory = wakeCategory;
    }

    /**
     * @return the mtow
     */
    public int getMtow() {
        return mtow;
    }

    /**
     * @param mtow the mtow to set
     */
    public void setMtow(int mtow) {
        this.mtow = mtow;
    }

    /**
     * @return the landingCategory
     */
    public List getLandingCategory() {
        return landingCategory;
    }

    /**
     * @param landingCategory the landingCategory to set
     */
    public void setLandingCategory(List landingCategory) {
        this.landingCategory = landingCategory;
    }

}
