/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.web;

import com.google.gson.Gson;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import org.flightgear.fgatc.AtnSession;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import org.flightgear.fgatc.AtnCommandMessage;
import org.flightgear.fgatc.AtnRequest;
import org.flightgear.fgatc.AtnRequestStatus;
import org.flightgear.fgatc.Station;
import org.flightgear.fgatc.ejb.AtcEJB;
import org.flightgear.fgatc.ejb.AtcSessionFacade;
import org.flightgear.fgatc.ejb.AtnSessionHolderEJB;
import org.flightgear.fgatc.ejb.entity.ATNMaintenanceRecord;
import org.flightgear.fgatc.ejb.entity.CompanyFPlanWP;
import org.flightgear.fgatc.web.atn.AtnLogonParams;

/**
 *
 * @author scotth
 */
@ManagedBean(name = "atn")
@RequestScoped
public class AtnBean {

    //@ManagedProperty("#{atnGlobalBean}")
    //private AtnGlobalBean global;
    @EJB
    AtnSessionHolderEJB global;
    @EJB
    AtcSessionFacade facade;
    @EJB
    AtcEJB atcBean;
    // working memory
    @ManagedProperty("#{param.xid}")
    private String sessionId;
    @ManagedProperty("#{param.user}")
    private String logonUser;
    @ManagedProperty("#{param.callsign}")
    private String logonCallsign;
    @ManagedProperty("#{param.airport}")
    private String logonAirport;
    @ManagedProperty("#{param.airline}")
    private String logonAirline;
    @ManagedProperty("#{param.type}")
    private String logonType;
    @ManagedProperty("#{param.maintRecord}")
    private String maintJSON;
    private String maintType;
    @ManagedProperty("#{param.positionRecord}")
    private String posJSON;
    @ManagedProperty("#{param.timestamp}")
    private String timestamp;
    @ManagedProperty("#{param.gate}")
    private String departGate;
    @ManagedProperty("#{param.depart}")
    private String departAirport;
    @ManagedProperty("#{param.arrival}")
    private String arrivalAirport;
    @ManagedProperty("#{atc}")
    private AtcBean atc;
    private AtnCommandMessage currentCommand;
    private AtnMaintCollection maintList;
    private AtnPositionRecord currentPosition;

    /**
     * Creates a new instance of atn
     */
    public AtnBean() {
    }

    /**
     * @return the atc
     */
    public AtcBean getAtc() {
        return atc;
    }

    /**
     * @param atc the atc to set
     */
    public void setAtc(AtcBean atc) {
        this.atc = atc;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the logonUser
     */
    public String getLogonUser() {
        return logonUser;
    }

    /**
     * @param logonUser the logonUser to set
     */
    public void setLogonUser(String logonUser) {
        this.logonUser = logonUser;
    }

    /**
     * @return the logonCallsign
     */
    public String getLogonCallsign() {
        return logonCallsign;
    }

    /**
     * @param logonCallsign the logonCallsign to set
     */
    public void setLogonCallsign(String logonCallsign) {
        this.logonCallsign = logonCallsign;
    }

    /**
     * @return the logonAirport
     */
    public String getLogonAirport() {
        return logonAirport;
    }

    /**
     * @param logonAirport the logonAirport to set
     */
    public void setLogonAirport(String logonAirport) {
        this.logonAirport = logonAirport;
    }

    /**
     * @return the logonAirline
     */
    public String getLogonAirline() {
        return logonAirline;
    }

    /**
     * @param logonAirline the logonAirline to set
     */
    public void setLogonAirline(String logonAirline) {
        this.logonAirline = logonAirline;
    }

    /**
     * @return the logonType
     */
    public String getLogonType() {
        return logonType;
    }

    /**
     * @param logonType the logonType to set
     */
    public void setLogonType(String logonType) {
        this.logonType = logonType;
    }

    /**
     * @return the maintJSON
     */
    public String getMaintJSON() {
        return maintJSON;
    }

    /**
     * @param maintJSON the maintJSON to set
     */
    public void setMaintJSON(String pJSON) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".setMainJSON");
        if (pJSON != null) {
            log.info("received JSON: " + pJSON);
            this.maintJSON = pJSON;
            Gson gson = new Gson();
            this.maintList = gson.fromJson(pJSON, AtnMaintCollection.class);
            log.info("Parsed " + this.maintList.records.length + " records..");
            this.maintType = this.maintList.records[0].getType();
        }
    }

    /**
     * @return the maintType
     */
    public String getMaintType() {
        return maintType;
    }

    /**
     * @param maintType the maintType to set
     */
    public void setMaintType(String maintType) {
        this.maintType = maintType;
    }

    /**
     * @return the posJSON
     */
    public String getPosJSON() {
        return posJSON;
    }

    /**
     * @param posJSON the posJSON to set
     */
    public void setPosJSON(String pJSON) {
        Logger log = Logger.getLogger(this.getClass().getName() + ".setPositionJSON");
        if (pJSON != null) {
            log.info("received JSON: " + pJSON);
            this.posJSON = pJSON;
            Gson gson = new Gson();
            this.currentPosition = gson.fromJson(pJSON, AtnPositionRecord.class);
        }
    }

    /**
     * @return the timestamp
     */
    public String getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the currentCommand
     */
    public AtnCommandMessage getCurrentCommand() {
        return currentCommand;
    }

    /**
     * @param currentCommand the currentCommand to set
     */
    public void setCurrentCommand(AtnCommandMessage currentCommand) {
        this.currentCommand = currentCommand;
    }

    /**
     * @return the departGate
     */
    public String getDepartGate() {
        return departGate;
    }

    /**
     * @param departGate the departGate to set
     */
    public void setDepartGate(String departGate) {
        this.departGate = departGate;
    }

    /**
     * @return the departAirport
     */
    public String getDepartAirport() {
        return departAirport;
    }

    /**
     * @param departAirport the departAirport to set
     */
    public void setDepartAirport(String departAirport) {
        this.departAirport = departAirport;
    }

    /**
     * @return the arrivalAirport
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * @param arrivalAirport the arrivalAirport to set
     */
    public void setArrivalAirport(String arrivalAirport) {
        this.arrivalAirport = arrivalAirport;
    }

    /**
     * @return the global
     */
    //public AtnGlobalBean getGlobal() {
    //    return global;
    //}
    /**
     * @param global the global to set
     */
    //public void setGlobal(AtnGlobalBean global) {
    //    this.global = global;
    //}
    public String getDoLogon() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doLogon");

        AtnSession session = new AtnSession();
        double rnd = Math.random();
        int xid = (int) Math.rint(rnd * 10000);
        session.sessionId = String.valueOf(xid);
        session.callSign = this.logonCallsign;
        session.aircraftType = this.logonType;
        log.info("logonType: " + this.logonType);
        session.closestAirport = this.logonAirport;
        log.info("logonCallsign: " + this.logonCallsign);
        session.user = this.logonUser;
        session.airline = this.logonAirline;
        session.lastContact = new Date();
        global.addSession(session);
        log.info("[ATN] created new session: " + session.sessionId + " for callsign: " + session.callSign);
        this.sessionId = session.sessionId;
        return "OK";
    }

    public String getDoLogoff() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doLogoff");
        String xid = this.sessionId;
        log.info("Logoff for session: " + xid);
        AtnSession session = global.findSession(xid);
        if (session != null) {
            global.removeSession(xid);
            return "OK";
        }
        return "NO SESSION";
    }

    public String getDoUpdateController() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doUpdateController");

        String xid = this.sessionId;
        log.info("Update airport for session: " + xid);
        AtnSession session = global.findSession(xid);
        String origAirport = session.closestAirport;
        session.closestAirport = this.logonAirport;
        this.logonAirport = origAirport;
        session.lastContact = new Date();
        global.addSession(session);
        return "OK";
    }

    public String getDoCheckCmdQueue() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doCheckCmdQueue");

        String xid = this.sessionId;
        log.info("PollCmd for session: " + xid);
        AtnSession session = global.findSession(xid);
        if (session != null) {
            log.info("Update session: " + xid);
            session.lastContact = new Date();
            if (session.getCmdQueue().size() > 0) {
                this.currentCommand = (AtnCommandMessage) session.getCmdQueue().pop();
                return this.currentCommand.getCommandMessage();
            } else {
                this.currentCommand = new AtnCommandMessage();
            }

            global.addSession(session);
            return "OK";
        }
        return "NO SESSION";
    }

    public String getDoReportMaintRecord() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doReportMaintRecord");

        String xid = this.sessionId;
        log.info("Maint for session: " + xid);
        AtnSession session = global.findSession(xid);
        session.lastContact = new Date();
        //try {
        SimpleDateFormat fgDate = new SimpleDateFormat("yyyy-mm-dd HH:MM:SS");
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        //cal.setTime(fgDate.parse(this.timestamp));
        //cal.setTime(new Date());

        for (int r = 0; r != this.maintList.records.length; r++) {
            AtnMaintRecord rec = this.maintList.records[r];
            ATNMaintenanceRecord record = new ATNMaintenanceRecord();
            record.setAircraftType(session.aircraftType);
            record.setAtnSessionId(session.sessionId);
            record.setAtnTime(cal.getTime());
            record.setAtnUser(session.user);
            record.setCallsign(session.callSign);
            record.setRecordType(rec.getType());
            record.setRecordKey(rec.getKey());
            record.setRecordValue(rec.getValue());
            facade.saveMaintRecord(record);
        }

        global.addSession(session);
        //} catch (ParseException ex) {
        //    Logger.getLogger(AtnBean.class.getName()).log(Level.SEVERE, null, ex);
        //}
        return "OK";
    }

    public String getDoRequestDepartClearance() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doDepartClearnace");
        String retStr = "REQUESTED";
        String xid = this.sessionId;
        log.info("Depart Clearance Request for session: " + xid);
        AtnSession session = global.findSession(xid);
        session.lastContact = new Date();
        AtnRequest req = new AtnRequest();
        req.requestType = "DEPART";
        req.session = session;
        req.setStatus("REQUESTED");
        req.requestParams.put("gate", this.departGate);
        req.requestParams.put("depart", this.departAirport);
        req.requestParams.put("arrival", this.arrivalAirport);
        List<Station> stationList = atcBean.findActiveStationByICAO(this.departAirport);
        if (stationList.size() > 0) {
            global.addRequest(req);
        } else {
            atcBean.addATCRequest(req);
        }

        log.info("Request status: " + req.getStatus() + ", request id: " + req.id);
        return retStr;
    }

    public String getDoPositionReport() {
        Logger log = Logger.getLogger(this.getClass().getName() + ".doPositionReport");
        String xid = this.sessionId;
        log.info("Position Report for session: " + xid);
        AtnSession session = global.findSession(xid);
        session.lastContact = new Date();
        session.posLat = this.currentPosition.posLat;
        session.posLon = this.currentPosition.posLon;
        session.altitudeFeet = this.currentPosition.altitudeFeet;
        session.altitudeMetres = this.currentPosition.altitudeMetres;
        return "OK";
    }
    
    public String doGetCompanyRoute() {
        return "OK";
    }
    
    public List<CompanyFPlanWP> getCurrentWaypoints() {
        List retList = new ArrayList();
        
        return retList;
    }
}
