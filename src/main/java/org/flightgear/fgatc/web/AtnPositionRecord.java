package org.flightgear.fgatc.web;

/**
 *
 * @author scotth
 */
public class AtnPositionRecord {
    public double posLat;
    public double posLon;
    public int    altitudeFeet;
    public int    altitudeMetres;
    public int    kias;

    /**
     * @return the posLat
     */
    public double getPosLat() {
        return posLat;
    }

    /**
     * @param posLat the posLat to set
     */
    public void setPosLat(double posLat) {
        this.posLat = posLat;
    }

    /**
     * @return the posLon
     */
    public double getPosLon() {
        return posLon;
    }

    /**
     * @param posLon the posLon to set
     */
    public void setPosLon(double posLon) {
        this.posLon = posLon;
    }

    /**
     * @return the altitudeFeet
     */
    public int getAltitudeFeet() {
        return altitudeFeet;
    }

    /**
     * @param altitudeFeet the altitudeFeet to set
     */
    public void setAltitudeFeet(int altitudeFeet) {
        this.altitudeFeet = altitudeFeet;
    }

    /**
     * @return the altitudeMetres
     */
    public int getAltitudeMetres() {
        return altitudeMetres;
    }

    /**
     * @param altitudeMetres the altitudeMetres to set
     */
    public void setAltitudeMetres(int altitudeMetres) {
        this.altitudeMetres = altitudeMetres;
    }

    /**
     * @return the kias
     */
    public int getKias() {
        return kias;
    }

    /**
     * @param kias the kias to set
     */
    public void setKias(int kias) {
        this.kias = kias;
    }
    
    
}
