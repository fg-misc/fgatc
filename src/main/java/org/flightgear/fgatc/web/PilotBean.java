/*
 *  Copyright (C) 2010 scotth
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import org.flightgear.fgatc.ejb.AtcSessionFacade;
import org.flightgear.fgatc.ejb.entity.FlightPlan;
import org.flightgear.fgatc.ejb.entity.User;

/**
 *
 * @author scotth
 */
@ManagedBean(name="pilot")
@SessionScoped
public class PilotBean implements Serializable {
    @EJB
    private AtcSessionFacade facadeEJB;
    private FlightPlan currentPlan;
    @ManagedProperty("#{user}")
    private UserBean usr;

    /** Constants **/

    HashMap aircraftAirspeedDefaults = new HashMap(40);
    HashMap aircraftFLDefaults = new HashMap(40);


    /** Creates a new instance of PilotBean */
    public PilotBean() {
        aircraftAirspeedDefaults.put("A380", "0.85");
        aircraftFLDefaults.put("A380", "380");
        aircraftAirspeedDefaults.put("A320", "0.80");
        aircraftFLDefaults.put("A320", "340");
        aircraftAirspeedDefaults.put("A340", "0.79");
        aircraftFLDefaults.put("A340", "360");
        aircraftAirspeedDefaults.put("A330", "0.80");
        aircraftFLDefaults.put("A330", "360");
        aircraftAirspeedDefaults.put("747", "0.83");
        aircraftFLDefaults.put("747", "380");
        aircraftAirspeedDefaults.put("737", "0.75");
        aircraftFLDefaults.put("737", "340");
        aircraftAirspeedDefaults.put("767", "0.80");
        aircraftFLDefaults.put("767", "360");
    }


    /**
     * @return the currentPlan
     */
    public FlightPlan getCurrentPlan() {
        return currentPlan;
    }

    /**
     * @param currentPlan the currentPlan to set
     */
    public void setCurrentPlan(FlightPlan currentPlan) {
        this.currentPlan = currentPlan;
    }

    /**
     * @return the usr
     */
    public UserBean getUsr() {
        return usr;
    }

    /**
     * @param usr the usr to set
     */
    public void setUsr(UserBean usr) {
        this.usr = usr;
    }


    public String readyNewPlan() {
        this.currentPlan = new FlightPlan();
        this.currentPlan.setDateCreated(new Date());
        this.currentPlan.setOriginatorUsername(usr.getUsername());
        User me = facadeEJB.findUser(usr.username);
        this.currentPlan.setPilotInCommand(me.getGivenName()+" "+me.getFamilyName());
        return "newPlan";
    }

    public String readyNewCompanyPlan() {
        return "newCompanyPlan";
    }

    public String loadAllPlans() {
        return "viewAllPlans";
    }

    public List getAllMyPlans() {
        return facadeEJB.findUserPlans(usr.getUsername());
    }

    public String savePlan() {
        this.currentPlan.setDateCreated(new Date());
        this.currentPlan.setOriginatorUsername(usr.getUsername());
        this.currentPlan.setPlanStatus("PENDING");
        facadeEJB.persist(this.currentPlan);
        return "index";
    }

    public void updatePlanAircraft(ValueChangeEvent e) {
        Logger log = Logger.getLogger(this.getClass().getName()+"updatePlanAircraft");
        UIInput nameInput = (UIInput)e.getComponent();
        String name = (String)nameInput.getValue();
        log.info("Update aircraft for: "+name+", currentType: "+currentPlan.getAircraftType());
        if (this.currentPlan.getCruiseSpeed() == null) {
            Double cruiseSpeed = Double.valueOf((String)aircraftAirspeedDefaults.get(name));
            this.currentPlan.setCruiseSpeed(cruiseSpeed);
            log.info("Updated cruiseSpeed to: "+cruiseSpeed);
        }
        if (this.currentPlan.getFlightLevel() == 0) {
            int flightLevel = Integer.valueOf((String)aircraftFLDefaults.get(name)).intValue();
            this.currentPlan.setFlightLevel(flightLevel);
            log.info("Updated FL to: "+flightLevel);
        }
    }

}
