/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.web;

import org.flightgear.fgatc.AtnSession;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author scotth
 */
// @ManagedBean(name = "atnGlobalBean", eager=true)
// @ApplicationScoped
public class AtnGlobalBean {
    private Map sessionList = new HashMap<String, AtnSession>();

    /**
     * Creates a new instance of AtnGlobalBean
     */
    public AtnGlobalBean() {
        Logger log = Logger.getLogger(this.getClass().getName()+".new");
        log.info("Create new AtnGlobalBean instance...");
    }

    /**
     * @return the sessionList
     */
    public Map getSessionList() {
        return sessionList;
    }

    /**
     * @param sessionList the sessionList to set
     */
    public void setSessionList(Map sessionList) {
        this.sessionList = sessionList;
    }
    
    public AtnSession findSession(String id) {
        Logger log = Logger.getLogger(this.getClass().getName()+".findSession");
        log.info("Search for session: "+id);
        for(Iterator s = this.sessionList.keySet().iterator(); s.hasNext();) {
            log.log(Level.INFO, "session key: {0}", s.next());
        }
        return (AtnSession)this.sessionList.get(id);
    }
    
    public void addSession(AtnSession sess) {
        Logger log = Logger.getLogger(this.getClass().getName()+".addSession");
        log.info("Update for session: "+sess.sessionId);
        this.sessionList.put(sess.sessionId, sess);
    }
}
