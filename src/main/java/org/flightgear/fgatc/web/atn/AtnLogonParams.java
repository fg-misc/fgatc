/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc.web.atn;

/**
 *
 * @author scotth
 */
public class AtnLogonParams {
    public String user;
    public String callsign;

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the callsign
     */
    public String getCallsign() {
        return callsign;
    }

    /**
     * @param callsign the callsign to set
     */
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }
    
}
