/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.flightgear.fgatc;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import org.primefaces.model.SelectableDataModel;

/**
 *
 * @author scotth
 */
public class AtnSession implements Serializable {
    public String sessionId;
    public String user;
    public String callSign;
    public String aircraftType;
    public String closestAirport;
    public String airline = "";
    public double  posLat;
    public double  posLon;
    public double altitudeFeet;
    public double altitudeMetres;
    public int    heading;
    public Date   lastContact;
    private LinkedList<AtnCommandMessage> cmdQueue = new LinkedList();

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    /**
     * @return the callSign
     */
    public String getCallSign() {
        return callSign;
    }

    /**
     * @param callSign the callSign to set
     */
    public void setCallSign(String callSign) {
        this.callSign = callSign;
    }

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the airline
     */
    public String getAirline() {
        return airline;
    }

    /**
     * @param airline the airline to set
     */
    public void setAirline(String airline) {
        this.airline = airline;
    }

    /**
     * @return the closestAirport
     */
    public String getClosestAirport() {
        return closestAirport;
    }

    /**
     * @param closestAirport the closestAirport to set
     */
    public void setClosestAirport(String closestAirport) {
        this.closestAirport = closestAirport;
    }

    /**
     * @return the posLat
     */
    public double getPosLat() {
        return posLat;
    }

    /**
     * @param posLat the posLat to set
     */
    public void setPosLat(double posLat) {
        this.posLat = posLat;
    }

    /**
     * @return the posLon
     */
    public double getPosLon() {
        return posLon;
    }

    /**
     * @param posLon the posLon to set
     */
    public void setPosLon(double posLon) {
        this.posLon = posLon;
    }

    /**
     * @return the altitudeFeet
     */
    public double getAltitudeFeet() {
        return altitudeFeet;
    }

    /**
     * @param altitudeFeet the altitudeFeet to set
     */
    public void setAltitudeFeet(double altitudeFeet) {
        this.altitudeFeet = altitudeFeet;
    }

    /**
     * @return the altitudeMetres
     */
    public double getAltitudeMetres() {
        return altitudeMetres;
    }

    /**
     * @param altitudeMetres the altitudeMetres to set
     */
    public void setAltitudeMetres(double altitudeMetres) {
        this.altitudeMetres = altitudeMetres;
    }

    /**
     * @return the lastContact
     */
    public Date getLastContact() {
        return lastContact;
    }

    /**
     * @param lastContact the lastContact to set
     */
    public void setLastContact(Date lastContact) {
        this.lastContact = lastContact;
    }

    /**
     * @return the cmdQueue
     */
    public LinkedList getCmdQueue() {
        return cmdQueue;
    }

    /**
     * @param cmdQueue the cmdQueue to set
     */
    public void setCmdQueue(LinkedList cmdQueue) {
        this.cmdQueue = cmdQueue;
    }
    
    
    
}
