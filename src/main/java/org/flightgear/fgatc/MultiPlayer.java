/*
 *  Copyright (C) 2010 S.Hamilton
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.flightgear.fgatc;

/**
 *
 * @author S.Hamilton
 */
public class MultiPlayer {
    public String callsign;
    public String atHost;
    public float  lat;
    public float  lon;
    public long   elevation;
    public float  heading;
    public String aircraftType;
    public String airspeed;   //either knots (250) or mach (eg: M0.85)
    public int    staleTime = 0;  //multiply by 10sec 

    /**
     * @return the callsign
     */
    public String getCallsign() {
        return callsign;
    }

    public String getCallsignID() {
        String str = this.callsign;
        str = str.replaceAll("[ \\t\\v\\(\\)\\/]", "");
        return str;
    }

    /**
     * @param callsign the callsign to set
     */
    public void setCallsign(String callsign) {
        this.callsign = callsign;
    }

    /**
     * @return the atHost
     */
    public String getAtHost() {
        return atHost;
    }

    /**
     * @param atHost the atHost to set
     */
    public void setAtHost(String atHost) {
        this.atHost = atHost;
    }

    /**
     * @return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        this.lat = lat;
    }

    /**
     * @return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return the elevation
     */
    public long getElevation() {
        return elevation;
    }

    /**
     * @param elevation the elevation to set
     */
    public void setElevation(long elevation) {
        this.elevation = elevation;
    }

    /**
     * @return the heading
     */
    public float getHeading() {
        return heading;
    }

    /**
     * @param heading the heading to set
     */
    public void setHeading(float heading) {
        this.heading = heading;
    }

    /**
     * @return the aircraftType
     */
    public String getAircraftType() {
        return aircraftType;
    }

    /**
     * @param aircraftType the aircraftType to set
     */
    public void setAircraftType(String aircraftType) {
        this.aircraftType = aircraftType;
    }

    /**
     * @return the airspeed
     */
    public String getAirspeed() {
        return airspeed;
    }

    /**
     * @param airspeed the airspeed to set
     */
    public void setAirspeed(String airspeed) {
        this.airspeed = airspeed;
    }
    

}
